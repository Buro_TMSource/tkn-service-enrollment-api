# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2018-05-22
### Released
- Basic configuration

## [1.0.1] - 2018-05-24
- Adds option for PictureController in order to obtain also the customer's selfie (option #4)
- Adds QR code generation with database support

## [1.0.2] - 2018-05-30
- Adds operation for getting selfie from remote quobis storage
- Adds configuration option for opening casefile without TAS. 
- Adds rest operation for b64 image for selfie (quobis)


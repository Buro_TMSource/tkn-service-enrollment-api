package com.teknei.bid.oauth.services;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Servicio para gestión de usuarios.
 * 
 * @author marojas
 *
 */
@Service
public class UserService implements UserDetailsService {
	private static final Logger log = LoggerFactory.getLogger(UserService.class);
	@Autowired
	private UserRepository userRepository;

	/**
	 * Obtiene un usuario en base a su nombre
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		//log.info("lblancas: "+this.getClass().getName()+".{loadUserByUsername() }");
		return userRepository.findOneByUsername(username);
	}

}

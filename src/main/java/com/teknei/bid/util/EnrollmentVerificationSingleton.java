package com.teknei.bid.util;

import com.teknei.bid.controller.rest.EnrollmentCredentialsController;
import com.teknei.bid.dto.RequestEnrollIneVerification;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EnrollmentVerificationSingleton {
	private static final Logger log = LoggerFactory.getLogger(EnrollmentVerificationSingleton.class);
    private static Map<Long, RequestEnrollIneVerification> requestMap;
    private static EnrollmentVerificationSingleton instance = new EnrollmentVerificationSingleton();

    private EnrollmentVerificationSingleton() {
        requestMap = new HashMap<>();
    }

    public static EnrollmentVerificationSingleton getInstance() {
    	//log.info("lblancas: EnrollmentVerificationSingleton.{EnrollmentVerificationSingleton() }");
        return instance;
    }

    public void addCustomerData(Long id, RequestEnrollIneVerification requestEnrollIneVerification) {
    	//log.info("lblancas: "+this.getClass().getName()+".{addCustomerData() }");
        requestMap.put(id, requestEnrollIneVerification);
    }

    public RequestEnrollIneVerification getCustomerData(Long id) {
    	//log.info("lblancas: "+this.getClass().getName()+".{getCustomerData() }");
        return requestMap.get(id);
    }

    public void removeData(Long id) {
    	//log.info("lblancas: "+this.getClass().getName()+".{removeData() }");
        requestMap.remove(id);
    }

}

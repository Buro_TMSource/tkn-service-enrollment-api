package com.teknei.bid.controller.rest.unauth;

import com.teknei.bid.controller.rest.EnrollmentBiometricIneProcess;
import com.teknei.bid.dto.OperationIdDTO;
import com.teknei.bid.dto.OperationResult;
import com.teknei.bid.dto.RequestEnrollIneVerification;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@RestController
@RequestMapping(value = "/rest/unauth/enrollment/biometricIne")
@CrossOrigin
public class EnrollmentBiometricIneProcessUnauth {
	private static final Logger log = LoggerFactory.getLogger(EnrollmentBiometricIneProcessUnauth.class);
    @Autowired
    private EnrollmentBiometricIneProcess controller;

    @ApiOperation(value = "Saves temporally the biometric data")
    @RequestMapping(value = "/addSlaps", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> addCaptureData(@RequestBody RequestEnrollIneVerification requestEnrollIneVerification, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{addCaptureData(POST) }");
        return controller.addCaptureData(requestEnrollIneVerification, request);
    }

    @ApiOperation(value = "Verifies the customer against INE-CECOBAN, returns institution response in raw")
    @RequestMapping(value = "/verifyFirst", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> verifyFirst(@RequestBody OperationIdDTO operationIdDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{verifyFirst(POST) }");
        return controller.verifyFirst(operationIdDTO, request);
    }

    @ApiOperation(value = "Confirms enrollment for opened process")
    @RequestMapping(value = "/confirmEnroll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> enrollCustomer(@RequestBody OperationIdDTO operationIdDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{enrollCustomer(POST) }");
        return controller.enrollCustomer(operationIdDTO, request);
    }

}

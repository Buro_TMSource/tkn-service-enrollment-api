package com.teknei.bid.controller.rest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.teknei.bid.controller.rest.util.crypto.TokenUtils;
import com.teknei.bid.dto.BidServiceConfiguration;
import com.teknei.bid.dto.ClieTelDTO;
import com.teknei.bid.dto.ClientDTO;
import com.teknei.bid.dto.ClientDetailDTO;
import com.teknei.bid.dto.OpenCasefileDTO;
import com.teknei.bid.dto.OperationResult;
import com.teknei.bid.dto.StartOperationResult;
import com.teknei.bid.dto.StepStatusDTO;
import com.teknei.bid.service.remote.CustomerClient;
import com.teknei.bid.service.validation.JsonValidation;
import com.teknei.bid.service.validation.OperationServices;

import feign.FeignException;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/rest/v3/enrollment/status")
@CrossOrigin
public class EnrollmentStatusController {

    @Autowired
    private OperationServices operationServices;
    @Autowired
    private CustomerClient customerClient;
    @Autowired
    private TokenUtils tokenUtils;

    private static final Logger log = LoggerFactory.getLogger(EnrollmentStatusController.class);

    @Value("${tkn.tas.active}")
    private boolean tasActive = true;
    
    @ApiOperation(value = "Finds the status map for accomplishment steps related to the enrollment process")
    @RequestMapping(value = "/accomplishment/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<StepStatusDTO>> findAccomplishmnet(@PathVariable Long id) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
        try {
            return customerClient.findAccomplishmnet(id);
        } catch (Exception e) {
            log.error("Error finding status relation for: {} with message: {}", id, e.getMessage());
            return new ResponseEntity<>((List<StepStatusDTO>) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }


    @ApiOperation(value = "Starts main process. Expects JSON string as main input like {'deviceId' : 'X' ,'employee' : 'X' , 'curp' : 'X', 'nombre' : 'X' , 'primerApellido' : 'X' , 'segundoApellido': ' X' , 'telefono'|'celular' : 'X' , 'email' : 'X' , 'emprId' : 'X', 'customerType': 1|2, 'confType' : 1|2 }", response = StartOperationResult.class)
    @RequestMapping(value = "/start", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.POST)
    public ResponseEntity<StartOperationResult> startOperation(
            @RequestBody String startOperationRequest, HttpServletRequest request) {
    	log.info("lblancas: "+startOperationRequest+"}");
    	log.info("Start operation request: {" + startOperationRequest+"}");
        StartOperationResult startOperationResult = new StartOperationResult();
        JSONObject startOperationRequestJSON = null;
        try {
            if (startOperationRequest != null) {
                startOperationRequestJSON = new JSONObject(startOperationRequest);
            }
        } catch (JSONException e) {
            startOperationResult.setErrorMessage(e.getMessage());
            return new ResponseEntity<>(startOperationResult, HttpStatus.BAD_REQUEST);
        }
        try {
            startOperationResult = operationServices.validateStartOperation(startOperationRequestJSON);
            String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
            if (startOperationResult.isResultOK()) {
                int confType = startOperationRequestJSON.getInt("confType");
                switch (confType) {
                    case 1:
                        return addClient(startOperationRequestJSON, username);
                    case 2:
                        return validateClient(startOperationRequestJSON, username);
                    default:
                        return addClient(startOperationRequestJSON, username);
                }
            } else {
                return new ResponseEntity<>(startOperationResult, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (IllegalArgumentException ie) {
            log.error("Error in startOperation for : {} with message: {}", startOperationRequest, ie.getMessage());
            startOperationResult.setErrorMessage(ie.getMessage());
            startOperationResult.setResultOK(false);
            return new ResponseEntity<>(startOperationResult, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
 
    @ApiOperation(value = "Gets the data {\"idCliente\":50206,\"idPerfil\":1} ", response = Boolean.class)
    @RequestMapping(value = "/updatePrfByClient", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public ResponseEntity<Boolean> updatePrfByClient(@RequestBody String searchDTO) 
    {
    	log.info("lblancas: OUATH "+this.getClass().getName()+".{updatePerfilByClient( "+searchDTO+") }");  
    	ResponseEntity<Boolean> dato =  customerClient.updateClientPerfil(searchDTO);
		return dato; 
    }
    
    @ApiOperation(value = "Starts main process. Expects JSON string as main input like {'deviceId' : 'X' ,'employee' : 'X' , 'curp' : 'X', 'nombre' : 'X' , 'primerApellido' : 'X' , 'segundoApellido': ' X' , 'telefono'|'celular' : 'X' , 'email' : 'X' , 'emprId' : 'X', 'customerType': 1|2, 'confType' : 1|2 }", response = StartOperationResult.class)
    @RequestMapping(value = "/startOperation", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.POST)
    public ResponseEntity<StartOperationResult> startOperationByPerfil(
            @RequestBody String startOperationRequest, HttpServletRequest request) {
    	log.info("lblancas: "+startOperationRequest+"}");
    	log.info("Start operation request: {" + startOperationRequest+"}");
        StartOperationResult startOperationResult = new StartOperationResult();
        JSONObject startOperationRequestJSON = null;
        try {
            if (startOperationRequest != null) {
                startOperationRequestJSON = new JSONObject(startOperationRequest);
            }
        } catch (JSONException e) {
            startOperationResult.setErrorMessage(e.getMessage());
            return new ResponseEntity<>(startOperationResult, HttpStatus.BAD_REQUEST);
        }
        try {
            startOperationResult = operationServices.validateStartOperation(startOperationRequestJSON);
            String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
            if (startOperationResult.isResultOK())  
            {
            	ResponseEntity<StartOperationResult> valida=null;
            	boolean existeCliente=false;
            	log.info("lblancas: Entra a validateClientByCurpAndPerfil ");
            	try
            	{
            		valida= validateClientByCurpAndPerfil(startOperationRequestJSON, username);
            		existeCliente=true;
            	}catch(Exception w)
            	{
            		existeCliente=false;
            	}
            	if(existeCliente && valida.getBody()!=null)
            	{
            		log.info("lblancas: Obtiene body valida ");
            		StartOperationResult startValida =valida.getBody(); 
            		log.info("lblancas: Dato con perfil "+startValida.getObs() );
            		log.info("lblancas: Dato con perfil "+startValida.getOperationId() );
            		log.info("lblancas: Dato con perfil "+startValida.getPerfil() );
            		log.info("lblancas: Dato con perfil "+startValida.getErrorMessage() );
        			return new ResponseEntity<>(startValida, HttpStatus.OK); 
            	}
            	else
            	{
            		log.info("lblancas: Obtiene alta");
        			ResponseEntity<StartOperationResult> alta = addClient(startOperationRequestJSON, username);
        			StartOperationResult startAlta =alta.getBody();
        			log.info("lblancas: Obtiene alta Entro a modificar");
        			if(startAlta.getOperationId()!=0)
        			{
        				int perfil=2;
        				long idClie=startAlta.getOperationId();
        				log.info("lblancas: Obtiene alta : perfil: "+perfil);
        				log.info("lblancas: Obtiene alta :idClie :"+idClie);
            			return new ResponseEntity<>(startAlta, HttpStatus.OK); 
        			}
            	}
            	  
            }
            else {
                return new ResponseEntity<>(startOperationResult, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (IllegalArgumentException ie) {
            log.error("Error in startOperation for : {} with message: {}", startOperationRequest, ie.getMessage());
            startOperationResult.setErrorMessage(ie.getMessage());
            startOperationResult.setResultOK(false);
            return new ResponseEntity<>(startOperationResult, HttpStatus.UNPROCESSABLE_ENTITY);
        }
		return new ResponseEntity<>(startOperationResult, HttpStatus.UNPROCESSABLE_ENTITY);
    }
      
    private boolean openRegProc(Long operationId, String username) {
    	//log.info("lblancas: "+this.getClass().getName()+".{openRegProc() }");
        OpenCasefileDTO openCasefileDTO = new OpenCasefileDTO();
        openCasefileDTO.setUsername(username);
        openCasefileDTO.setUserOpeCrea(username);
        openCasefileDTO.setOperationId(operationId);
        try {
            ResponseEntity<Boolean> responseEntity = customerClient.openRegProc(openCasefileDTO);
            if (!responseEntity.getBody()) {
                log.error("Error opening RegProc for: {}", openCasefileDTO);
            }
            return responseEntity.getBody();
        } catch (Exception e) {
            log.error("Error opening RegProc for: {} with message: {}", openCasefileDTO, e.getMessage());
            return false;
        }

    }

    
    /**
     * Validates information pre-registered in database
     *
     * @param jsonObject
     * @return
     */
    private ResponseEntity<StartOperationResult> validateClient(JSONObject jsonObject, String username) {
    	//log.info("lblancas: "+this.getClass().getName()+".{validateClient() }");
        String curp = jsonObject.getString("curp");
        StartOperationResult operationResult = new StartOperationResult();
        try {
            ResponseEntity<ClientDetailDTO> responseEntity = customerClient.findDetailCurp(curp);
            JSONObject jsonPart = new JSONObject();
            ClientDetailDTO clientDetailDTO = responseEntity.getBody();
            openRegProc(clientDetailDTO.getId(), "tkn-api");
            jsonPart.put("name", clientDetailDTO.getName());
            jsonPart.put("surnameFirst", clientDetailDTO.getSurnameFirst());
            jsonPart.put("surnameLast", clientDetailDTO.getSurnameLast());
            jsonPart.put("email", clientDetailDTO.getEmail());
            List<ClieTelDTO> tels = clientDetailDTO.getTelephones();
            ClieTelDTO cel = null;
            if (tels != null) {
                List<ClieTelDTO> listCels = tels.stream().filter(t -> t.getType() == 0).collect(Collectors.toList());
                if (listCels != null && !listCels.isEmpty()) {
                    cel = listCels.get(0);
                }
            }
            jsonPart.put("tel", cel == null ? "" : cel.getTel()); 
            try {
                ResponseEntity<Integer> responseEntity1 = customerClient.findStepFromReferenceCurp(curp);
                jsonPart.put("step", responseEntity1.getBody());
            } catch (FeignException e) {
                jsonPart.put("step", 0);
            }
            operationResult.setErrorMessage("Proceso activo para cliente enrolado con curp: " + curp);
            operationResult.setOperationId(clientDetailDTO.getId());
            operationResult.setObs(jsonPart.toString());
            operationResult.setResultOK(true);
            OpenCasefileDTO openCasefileDTO = new OpenCasefileDTO();
            openCasefileDTO.setOperationId(responseEntity.getBody().getId());
            openCasefileDTO.setUsername(username);
            openCasefileDTO.setUserOpeCrea(username);
            try {
                customerClient.findCasefile(openCasefileDTO);
            } catch (FeignException e) {
                if (e.status() == 404) {
                    log.info("No casefile found for customer: {} , opening one", jsonObject.toString());
                    customerClient.openCasefile(openCasefileDTO);
                } else {
                    log.error("No document manager available to open casefile");
                    //TODO check what to do if the casefile could not be opened
                }
            } catch (Exception e2) {
                log.error("Error finding casefile for customer: {} with message: {}", jsonObject.toString(), e2.getMessage());
                //TODO check what to do if the casefile could not be opened
            }
            return new ResponseEntity<>(operationResult, HttpStatus.OK);
        } catch (FeignException e) {
            if (e.status() == 404) {
                operationResult.setErrorMessage("Proceso no activo para cliente con curp: " + curp);
                operationResult.setOperationId(0);
                operationResult.setResultOK(false);
                return new ResponseEntity<>(operationResult, HttpStatus.NOT_FOUND);
            } else {
                operationResult.setErrorMessage("Error en proceso para cliente con curp: " + curp);
                operationResult.setOperationId(0);
                operationResult.setResultOK(false);
                return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        }

    } 
    /**
     * Validates information pre-registered in database
     *
     * @param jsonObject
     * @return
     */
    
    private ResponseEntity<StartOperationResult> validateClientByCurpAndPerfil(JSONObject jsonObject, String username) {
    	log.info("lblancas: "+this.getClass().getName()+".{validateClientByCurpAndPerfil() }");
        String curp = jsonObject.getString("curp");
        String perfil="2";
        boolean existe=false;
        StartOperationResult operationResult = new StartOperationResult();
        try { 
        	JSONObject jsonPart = new JSONObject();
        	log.info("lblancas Lo busco : Envio la busqueda por perfil 2");
        	ResponseEntity<ClientDetailDTO> responseEntity =null;
        	try
        	{
        		responseEntity = customerClient.FindDetailByCurpAndPerfil(curp,perfil);
        		existe=true;
        	}
        	catch(Exception e)
        	{
        		try
        		{
        		  perfil="3";
	           	  log.info("lblancas Lo busco : Envio la busqeuda por perfil 3");
	           	  responseEntity = customerClient.FindDetailByCurpAndPerfil(curp,perfil);
	           	existe =true;
        		}
        		catch(Exception e1)
        		{
        			
        		}
        	}
        	
            if(!existe)
            	return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            if(responseEntity==null && (!existe))
            	return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            if(responseEntity.getBody()==null && (!existe))
            	return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            log.info("lblancas Datos existentes");
            ClientDetailDTO clientDetailDTO = responseEntity.getBody();
            openRegProc(clientDetailDTO.getId(), "tkn-api");
            jsonPart.put("name", clientDetailDTO.getName());
            jsonPart.put("surnameFirst", clientDetailDTO.getSurnameFirst());
            jsonPart.put("surnameLast", clientDetailDTO.getSurnameLast());
            jsonPart.put("email", clientDetailDTO.getEmail());
            List<ClieTelDTO> tels = clientDetailDTO.getTelephones();
            ClieTelDTO cel = null;
            if (tels != null) {
                List<ClieTelDTO> listCels = tels.stream().filter(t -> t.getType() == 0).collect(Collectors.toList());
                if (listCels != null && !listCels.isEmpty()) {
                    cel = listCels.get(0);
                }
            }
            jsonPart.put("tel", cel == null ? "" : cel.getTel());
            try {
                ResponseEntity<Integer> responseEntity1 = customerClient.findStepFromReferenceCurp(curp);
                jsonPart.put("step", responseEntity1.getBody());
            } catch (FeignException e) {
                jsonPart.put("step", 0);
            }
            operationResult.setErrorMessage("Proceso activo para cliente enrolado con curp: " + curp);
            operationResult.setOperationId(clientDetailDTO.getId());
            operationResult.setObs(jsonPart.toString());
            operationResult.setResultOK(true);
            String perfilSTR="001";
            if(perfil.equals("2")) perfilSTR="002";
            if(perfil.equals("3")) perfilSTR="003";
            operationResult.setPerfil(perfilSTR);
            
            OpenCasefileDTO openCasefileDTO = new OpenCasefileDTO();
            openCasefileDTO.setOperationId(responseEntity.getBody().getId());
            openCasefileDTO.setUsername(username);
            openCasefileDTO.setUserOpeCrea(username);
            
            try {
                customerClient.findCasefile(openCasefileDTO);
            } catch (FeignException e) {
                if (e.status() == 404) {
                    log.info("No casefile found for customer: {} , opening one", jsonObject.toString());
                    customerClient.openCasefile(openCasefileDTO);
                } else {
                    log.error("No document manager available to open casefile");
                    //TODO check what to do if the casefile could not be opened
                }
            } catch (Exception e2) {
                log.error("Error finding casefile for customer: {} with message: {}", jsonObject.toString(), e2.getMessage());
                //TODO check what to do if the casefile could not be opened
            }
            return new ResponseEntity<>(operationResult, HttpStatus.OK);
        } catch (FeignException e) {
            if (e.status() == 404) {
                operationResult.setErrorMessage("Proceso no activo para cliente con curp: " + curp);
                operationResult.setOperationId(0);
                operationResult.setResultOK(false);
                return new ResponseEntity<>(operationResult, HttpStatus.NOT_FOUND);
            } else {
                operationResult.setErrorMessage("Error en proceso para cliente con curp: " + curp);
                operationResult.setOperationId(0);
                operationResult.setResultOK(false);
                return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        }

    }

    /**
     * Adds customer data to local storage
     *
     * @param startOperationRequestJSON
     * @return
     */
    private ResponseEntity<StartOperationResult> addClient(JSONObject startOperationRequestJSON, String username) {
    	//log.info("lblancas: "+this.getClass().getName()+".{addClient() }");
        ClientDTO clientDTO = new ClientDTO();
        clientDTO.setName(startOperationRequestJSON.getString("nombre"));
        clientDTO.setSurnameFirst(startOperationRequestJSON.getString("primerApellido"));
        clientDTO.setSurnameLast(startOperationRequestJSON.getString("segundoApellido"));
        clientDTO.setEmail(startOperationRequestJSON.getString("email"));
        clientDTO.setCurp(startOperationRequestJSON.getString("curp"));
        Integer idType = startOperationRequestJSON.getInt("customerType");
        List<Integer> idTypes = new ArrayList<>();
        idTypes.add(idType);
        clientDTO.setIdClientType(idTypes);
        clientDTO.setUsername("api-bid");
        clientDTO.setEmprId(startOperationRequestJSON.getLong("emprId"));
        ClieTelDTO clieTelDTO = new ClieTelDTO();
        if (startOperationRequestJSON.has("telefono")) {
            clieTelDTO.setTel(startOperationRequestJSON.getString("telefono"));
            clieTelDTO.setType(1);
        } else if (startOperationRequestJSON.has("celular")) {
            clieTelDTO.setTel(startOperationRequestJSON.getString("celular"));
            clieTelDTO.setType(0);	
        }
        clientDTO.setTelephones(Arrays.asList(clieTelDTO));
        clientDTO.setUsername(username);
        ResponseEntity<ClientDTO> responseEntity = null;
        StartOperationResult operationResult = new StartOperationResult();
        try {
            log.info("Adding customer with data: {}", clientDTO);
            responseEntity = customerClient.addClient(clientDTO);
            OpenCasefileDTO openCasefileDTO = new OpenCasefileDTO();
            openCasefileDTO.setOperationId(responseEntity.getBody().getId());
            openCasefileDTO.setUsername(username);
            openCasefileDTO.setUserOpeCrea(username);
            if (tasActive) {
                log.info("Adding casefile with data: {}", openCasefileDTO);
                customerClient.openCasefile(openCasefileDTO);
            	
            } else {
            	log.info("TAS no active");
                
            }
            openRegProc(openCasefileDTO.getOperationId(), openCasefileDTO.getUsername());
        } catch (FeignException e) {
            if (e.status() >= 400 && e.status() < 500) {
                operationResult.setResultOK(false);
                operationResult.setErrorMessage("Ya se encuentra un proceso activo para el curp:" + startOperationRequestJSON.getString("curp"));
                return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
            } else {
                operationResult.setResultOK(false);
                operationResult.setErrorMessage("Error al iniciar proceso para el curp:" + startOperationRequestJSON.getString("curp"));
                return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            log.error("Error in saveClient data for customer: {} with message: {}", clientDTO, e.getMessage());
            operationResult.setResultOK(false);
            operationResult.setErrorMessage("Error al iniciar proceso para el curp:" + startOperationRequestJSON.getString("curp"));
            return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
        }
        operationResult.setOperationId(responseEntity.getBody().getId());
        log.info("Process 'startOperation' is successfully for id: {}", responseEntity.getBody().getId());
        operationResult.setErrorMessage("Se ha iniciado el proceso de un nuevo enrolamiento: " + startOperationRequestJSON.getString("curp"));
        operationResult.setResultOK(true);
        return new ResponseEntity<>(operationResult, HttpStatus.OK);
    }

    @ApiOperation(value = "Terminates current opetation, expects a JSON string as input with {'operationId' : 'x'}", response = OperationResult.class)
    @RequestMapping(value = "/end", method = RequestMethod.POST, consumes = "application/json;charset=utf-8", produces = "application/json;charset=utf-8")
    public ResponseEntity<OperationResult> endOperation(
            @RequestBody String endOperationRequest) throws Exception {
    	//log.info("lblancas: "+this.getClass().getName()+".{endOperation() }");
        OperationResult operationResult = new OperationResult();
        JSONObject endOperationRequestJSON = null;
        if (!JsonValidation.validateJson(endOperationRequest)) {
            operationResult.setErrorMessage("Bad Request");
            return new ResponseEntity<>(operationResult, HttpStatus.BAD_REQUEST);
        }
        try {
            //TODO verificar si la operacion ya estaba en BD a partir del id
            operationResult.setResultOK(true);
            operationResult.setErrorMessage("Se ha terminado el proceso de enrolamiento");
            return new ResponseEntity<>(operationResult, HttpStatus.OK);
        } catch (Exception e) {
            operationResult.setErrorMessage(e.getMessage());
            return new ResponseEntity<>(operationResult, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Cancels the active operation, expects the operation Id as input", response = OperationResult.class)
    @RequestMapping(value = "/cancel", method = RequestMethod.DELETE, produces = "application/json;charset=utf-8")
    public ResponseEntity<OperationResult> cancelOperation(
            @RequestParam long operationId) {
    	//log.info("lblancas: "+this.getClass().getName()+".{cancelOperation() }");
        OperationResult operationResult = new OperationResult();
        //TODO solo veriricar contra bd y cancelar la operacion en bitacora
        operationResult.setErrorMessage("Se ha cancelado la operacion de enrolamiento");
        operationResult.setResultOK(true);
        return new ResponseEntity<>(operationResult, HttpStatus.OK);
    }
}

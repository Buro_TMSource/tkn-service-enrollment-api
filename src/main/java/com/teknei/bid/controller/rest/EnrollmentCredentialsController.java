package com.teknei.bid.controller.rest;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.teknei.bid.controller.rest.util.crypto.Decrypt;
import com.teknei.bid.controller.rest.util.crypto.TokenUtils;
import com.teknei.bid.dto.BasicRequestDTO;
import com.teknei.bid.dto.BiometricComplexResponse;
import com.teknei.bid.dto.CurpRequestDTO;
import com.teknei.bid.dto.IneDetailDTO;
import com.teknei.bid.dto.OperationResult;
import com.teknei.bid.dto.PersonData;
import com.teknei.bid.dto.PersonDataIneTKNRequest;
import com.teknei.bid.dto.PersonDataIneTKNRequestService;
import com.teknei.bid.dto.RequestEncFilesDTO;
import com.teknei.bid.service.remote.BiometricClient;
import com.teknei.bid.service.remote.IdentificationClient;
import com.teknei.bid.service.remote.impl.IdentificationAttachmentsClient;
import com.teknei.bid.service.validation.JsonValidation;
import feign.FeignException;
import io.swagger.annotations.ApiOperation;
@RestController
@RequestMapping(value = "/rest/v3/enrollment/credentials")
@CrossOrigin
public class EnrollmentCredentialsController {

    @Autowired
    private IdentificationAttachmentsClient identificationAttachmentsClient;
    @Autowired
    private IdentificationClient identificationClient;
    @Autowired
    private BiometricClient biometricClient;
    @Autowired
    private Decrypt decrypt;
    @Autowired
    private TokenUtils tokenUtils;
      
    
    private static final Logger log = LoggerFactory.getLogger(EnrollmentCredentialsController.class);

    @ApiOperation(value = "Verifies the curp related to the customer. The request could be by customer internal identification or by external data")
    @RequestMapping(value = "/curp/validate", method = RequestMethod.POST)
    public ResponseEntity<String> validateCurp(@RequestBody CurpRequestDTO curpRequestDTO, HttpServletRequest request){
    	//log.info("lblancas: "+this.getClass().getName()+".{validateCurp() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        try{
            return identificationClient.validateCurp(curpRequestDTO);
        }catch (FeignException fe){
            if(fe.status() == 404){
                return new ResponseEntity<>((String) null, HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>((String) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Obtain the curp related to the customer. The request could be by customer internal identification or by external data")
    @RequestMapping(value = "/curp/obtain", method = RequestMethod.POST)
    public ResponseEntity<String> getCurp(@RequestBody CurpRequestDTO curpRequestDTO, HttpServletRequest request){
    	//log.info("lblancas: "+this.getClass().getName()+".{getCurp() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        try{
            return identificationClient.getCurp(curpRequestDTO);
        }catch (FeignException fe){
            if(fe.status() == 404){
                return new ResponseEntity<>((String) null, HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>((String) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }


    @ApiOperation(value = "Verifies the information against INE CECOBAN stage", response = String.class)
    @RequestMapping(value = "/verifyCecoban", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> verifyAgainstCecoban(@RequestBody PersonDataIneTKNRequest personDataIneTKNRequest, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{verifyAgainstCecoban() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        try {
            ResponseEntity<BiometricComplexResponse> responseResponseEntity = biometricClient.findDetailRecord(String.valueOf(personDataIneTKNRequest.getId()));
            BiometricComplexResponse complexResponse = responseResponseEntity.getBody();
            if (!(complexResponse.getRightIndex() == null || complexResponse.getRightIndex().isEmpty())) {
                personDataIneTKNRequest.setRightIndexB64(complexResponse.getRightIndex());
            }
            if (!(complexResponse.getLeftIndex() == null || complexResponse.getLeftIndex().isEmpty())) {
                personDataIneTKNRequest.setLeftIndexB64(complexResponse.getLeftIndex());
            }
            PersonDataIneTKNRequestService service = new PersonDataIneTKNRequestService();
            service.setId(personDataIneTKNRequest.getId());
            service.setLeftIndexB64(personDataIneTKNRequest.getLeftIndexB64());
            service.setRightIndexB64(personDataIneTKNRequest.getRightIndexB64());
            service.setUsername(username);
            return identificationClient.verifyAgainstINE(service);
        } catch (Exception e) {
            log.error("Error invoking cecoban with message: {}", e.getMessage());
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Error", e.getMessage());
            return new ResponseEntity<>(jsonObject.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @Deprecated
    @RequestMapping(value = "/verify", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> verifyAgainstINE(@RequestBody PersonData personData) {
    	//log.info("lblancas: "+this.getClass().getName()+".{verifyAgainstINE() }");
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("desc", "OCR_VIGENTE");
            jsonObject.put("simi", "100");
            jsonObject.put("code", "9_1");
            jsonObject.put("apellidoPaterno", personData.getSurename());
            jsonObject.put("apellidoMaterno", personData.getSurenameLast());
            jsonObject.put("nombre", personData.getName());
            jsonObject.put("digSalidaDatos", UUID.randomUUID().toString());
            return new ResponseEntity<>(jsonObject.toString(), HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error finding INE verification for: {} with message: {}", personData, e.getMessage());
            return new ResponseEntity<>((String) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @RequestMapping(value = "/credential/findDetail/{type}/{id}", method = RequestMethod.GET)
    public ResponseEntity<IneDetailDTO> findDetail(@PathVariable Integer type, @PathVariable Long id) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findDetail() }");
        if (type.equals(2)) {
            return new ResponseEntity<>((IneDetailDTO) null, HttpStatus.NOT_IMPLEMENTED);
        }
        try {
            return identificationClient.findDetail(id);
        } catch (Exception e) {
            log.error("Error invoking findDetail for: {} with message: {}", id, e.getMessage());
            return new ResponseEntity<>((IneDetailDTO) null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Updates the information given by parsing systems. The user field is the logged user", response = OperationResult.class)
    @RequestMapping(value = "/credential/update/{type}/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> updateCredentialData(@PathVariable Integer type, @PathVariable Long id, @RequestBody IneDetailDTO ineDetailDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{updateCredentialData() }");
    	String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        if (!type.equals(1)) {
            //TODO update Passport data
            OperationResult operationResult = new OperationResult();
            operationResult.setResultOK(true);
            operationResult.setErrorMessage("OK");
            return new ResponseEntity<>(operationResult, HttpStatus.OK);
        }
        OperationResult operationResult = new OperationResult();
        try {
            ineDetailDTO.setUser(username);
            ResponseEntity<String> responseEntity = identificationClient.updateCredentialData(id, ineDetailDTO);
            operationResult.setErrorMessage(responseEntity.getBody());
            if (responseEntity.getStatusCode().is2xxSuccessful()) {
                operationResult.setResultOK(true);
                return new ResponseEntity<>(operationResult, HttpStatus.OK);
            } else {
                operationResult.setResultOK(false);
                return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            log.error("Error calling update detail for: {} {} {} with message: {}", type, id, ineDetailDTO, e.getMessage());
            operationResult.setResultOK(false);
            operationResult.setErrorMessage("Error in update resources");
            return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Stores and validates the credentials sent as attachment list, also expects a JSON like {'operationId' : 1}", response = OperationResult.class)
    @RequestMapping(value = "/credentialsAdditional", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<OperationResult> additionalCredentialsOperation(@RequestPart(value = "json") String additionalCredentialsOperationRequest,
                                                                          @RequestPart(value = "file", required = false) List<MultipartFile> files) {
    	//log.info("lblancas: "+this.getClass().getName()+".{additionalCredentialsOperation() }");
        OperationResult operationResult = new OperationResult();
        JSONObject credentialsOperationRequestJSON = null;
        if (!JsonValidation.validateJson(additionalCredentialsOperationRequest)) {
            operationResult.setErrorMessage("Bad Request");
            return new ResponseEntity<>(operationResult, HttpStatus.BAD_REQUEST);
        }
        credentialsOperationRequestJSON = new JSONObject(additionalCredentialsOperationRequest);
        Long operationId = credentialsOperationRequestJSON.getLong("operationId");
        try {
            ResponseEntity<String> responseEntity = identificationAttachmentsClient.uploadAdditonalIdentification(files, operationId);
            if (responseEntity == null) {
                operationResult.setResultOK(false);
                operationResult.setErrorMessage("10001");
                return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
            } else if (responseEntity.getStatusCode().is4xxClientError() || responseEntity.getStatusCode().is5xxServerError()) {
                operationResult.setResultOK(false);
                operationResult.setErrorMessage(responseEntity.getBody());
                return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
            } else {
                operationResult.setResultOK(true);
                operationResult.setErrorMessage("Segunda credencial almacenada correctamente");
                return new ResponseEntity<>(operationResult, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("Error adding additional identifications with message: {}", e.getMessage());
            e.printStackTrace();
            operationResult.setResultOK(false);
            operationResult.setErrorMessage("10001");
            return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }


    @ApiOperation(value = "Stores and validates the result of the credentials validation, also expects a JSON like {'operationId' : 1}", response = OperationResult.class)
    @RequestMapping(value = "/credentialsValidation", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<OperationResult> credentialsValidationOperation(@RequestPart(value = "json") String additionalCredentialsOperationRequest,
                                                                          @RequestPart(value = "file", required = false) List<MultipartFile> files) {
    	//log.info("lblancas: "+this.getClass().getName()+".{credentialsValidationOperation() }");
        OperationResult operationResult = new OperationResult();
        JSONObject credentialsOperationRequestJSON = null;
        if (!JsonValidation.validateJson(additionalCredentialsOperationRequest)) {
            operationResult.setErrorMessage("Bad Request");
            return new ResponseEntity<>(operationResult, HttpStatus.BAD_REQUEST);
        }
        credentialsOperationRequestJSON = new JSONObject(additionalCredentialsOperationRequest);
        Long operationId = credentialsOperationRequestJSON.getLong("operationId");
        try {
            ResponseEntity<String> responseEntity = identificationAttachmentsClient.uploadIdentificationValidation(files, operationId);
            if (responseEntity == null) {
                operationResult.setResultOK(false);
                operationResult.setErrorMessage("10001");
                return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
            } else if (responseEntity.getStatusCode().is4xxClientError() || responseEntity.getStatusCode().is5xxServerError()) {
                operationResult.setResultOK(false);
                operationResult.setErrorMessage(responseEntity.getBody());
                return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
            } else {
                operationResult.setResultOK(true);
                operationResult.setErrorMessage("Validacion de credenciales almacenada correctamente");
                return new ResponseEntity<>(operationResult, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("Error adding additional identifications with message: {}", e.getMessage());
            e.printStackTrace();
            operationResult.setResultOK(false);
            operationResult.setErrorMessage("10001");
            return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
        }


    }

    @ApiOperation(value = "Stores and validates credentials in background mode", response = OperationResult.class)
    @RequestMapping(value = "/credentialAsync", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> credentialsAsync(@RequestBody RequestEncFilesDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        dto.setUsername(username);
        Thread t = buildThread(dto, false, UploadPlainType.PRIMARY_ID);
        t.start();
        return getAsyncOKResponse();
    }

    @ApiOperation(value = "Stores and validates credentials in background mode. Ciphered endpoint", response = OperationResult.class)
    @RequestMapping(value = "/credentialAsyncCiphered", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> credentialsAsyncCiphered(@RequestBody RequestEncFilesDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        dto.setUsername(username);
        Thread t = buildThread(dto, true, UploadPlainType.PRIMARY_ID);
        t.start();
        return getAsyncOKResponse();
    }

    @ApiOperation(value = "Stores and validates credentials evidences in background mode", response = OperationResult.class)
    @RequestMapping(value = "/credentialCaptureAsync", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> credentialsCaptureAsync(@RequestBody RequestEncFilesDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        dto.setUsername(username);
        Thread t = buildThread(dto, false, UploadPlainType.ADDITIONAL_RESOURCES);
        t.start();
        return getAsyncOKResponse();
    }

    @ApiOperation(value = "Stores and validates credentials evidences in background mode. Ciphered endpoint", response = OperationResult.class)
    @RequestMapping(value = "/credentialCaptureAsyncCiphered", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> credentialsCaptureAsyncCiphered(@RequestBody RequestEncFilesDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        dto.setUsername(username);
        Thread t = buildThread(dto, true, UploadPlainType.ADDITIONAL_RESOURCES);
        t.start();
        return getAsyncOKResponse();
    }

    @ApiOperation(value = "Stores and validates additional credentials in background mode", response = OperationResult.class)
    @RequestMapping(value = "/credentialAdditionalAsync", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> credentialsAdditionalAsync(@RequestBody RequestEncFilesDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        dto.setUsername(username);
        Thread t = buildThread(dto, false, UploadPlainType.SECONDARY_ID);
        t.start();
        return getAsyncOKResponse();
    }

    @ApiOperation(value = "Stores and validates additional credentials in background mode. Ciphered endpoint", response = OperationResult.class)
    @RequestMapping(value = "/credentialAdditionalAsyncCiphered", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> credentialsAdditionalAsyncCiphered(@RequestBody RequestEncFilesDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        dto.setUsername(username);
        Thread t = buildThread(dto, true, UploadPlainType.SECONDARY_ID);
        t.start();
        return getAsyncOKResponse();
    }

    private Thread buildThread(RequestEncFilesDTO dto, boolean ciphered, UploadPlainType type) {
    	//log.info("lblancas: "+this.getClass().getName()+".{buildThread() }");
        Runnable target = () -> {
            try {
                if (ciphered) {
                    String targetStr = decrypt.decrypt(dto.getB64Anverse());
                    String anverse = new String(targetStr);
                    String reverse = null;
                    if (dto.getB64Reverse() != null && !dto.getB64Reverse().isEmpty()) {
                        String targetStrR = decrypt.decrypt(dto.getB64Reverse());
                        reverse = new String(targetStrR);
                    }
                    dto.setB64Anverse(anverse);
                    dto.setB64Reverse(reverse);
                }
                switch (type) {
                    case PRIMARY_ID:
                        identificationClient.uploadCredentialsAsync(dto);
                        break;
                    case SECONDARY_ID:
                        identificationClient.uploadAdditionalCredentials(dto);
                        break;
                    case ADDITIONAL_RESOURCES:
                        identificationClient.uploadCaptureCredentials(dto);
                        break;
                    default:
                        break;
                }
            } catch (Exception e) {
                log.error("Error uploading async additional credential in plain with message: {}", e.getMessage());
            }
        };
        Thread t = new Thread(target);
        return t;
    }

    public enum UploadPlainType {
        PRIMARY_ID, SECONDARY_ID, ADDITIONAL_RESOURCES
    }

    private ResponseEntity<OperationResult> getAsyncOKResponse() {
    	//log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
        OperationResult operationResult = new OperationResult();
        operationResult.setErrorMessage("BOOKED");
        operationResult.setResultOK(true);
        return new ResponseEntity<>(operationResult, HttpStatus.OK);
    }

    @RequestMapping(value = "/uploadPlainCiphered", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> uploadPlainEnc(@RequestBody RequestEncFilesDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{uploadPlainEnc() }");
        if(dto.getB64Reverse() != null && !dto.getB64Reverse().isEmpty()){
            dto.setB64Reverse(decrypt.decrypt(dto.getB64Reverse()));
        }
        if(dto.getB64Anverse() != null && !dto.getB64Anverse().isEmpty()){
            dto.setB64Anverse(decrypt.decrypt(dto.getB64Anverse()));
        }
        return uploadPlain(dto, request);
    }


    @RequestMapping(value = "/uploadPlain", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> uploadPlain(@RequestBody RequestEncFilesDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{uploadPlain() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        dto.setUsername(username);
        OperationResult operationResult = new OperationResult();
        try {
            ResponseEntity<String> responseEntity = identificationClient.uploadCredentialsAsync(dto);
            operationResult.setResultOK(true);
            String detailResult = null;
            try {
                ResponseEntity<IneDetailDTO> responseEntityDetail = identificationClient.findDetail(dto.getOperationId());
                if (responseEntityDetail != null && responseEntityDetail.getStatusCode().is2xxSuccessful()) {
                    IneDetailDTO detailDTO = responseEntityDetail.getBody();
                    JSONObject detailJson = new JSONObject();
                    detailJson.put("apeMat", detailDTO.getApeMat() == null ? JSONObject.NULL : detailDTO.getApeMat());
                    detailJson.put("apePat", detailDTO.getApePat() == null ? JSONObject.NULL : detailDTO.getApePat());
                    detailJson.put("call", detailDTO.getCall() == null ? JSONObject.NULL : detailDTO.getCall());
                    detailJson.put("clavElec", detailDTO.getClavElec() == null ? JSONObject.NULL : detailDTO.getClavElec());
                    detailJson.put("col", detailDTO.getCol() == null ? JSONObject.NULL : detailDTO.getCol());
                    detailJson.put("cp", detailDTO.getCp() == null ? JSONObject.NULL : detailDTO.getCp());
                    detailJson.put("dist", detailDTO.getDist() == null ? JSONObject.NULL : detailDTO.getDist());
                    detailJson.put("esta", detailDTO.getEsta() == null ? JSONObject.NULL : detailDTO.getEsta());
                    detailJson.put("foli", detailDTO.getFoli() == null ? JSONObject.NULL : detailDTO.getFoli());
                    detailJson.put("loca", detailDTO.getLoca() == null ? JSONObject.NULL : detailDTO.getLoca());
                    detailJson.put("mrz", detailDTO.getMrz() == null ? JSONObject.NULL : detailDTO.getMrz());
                    detailJson.put("muni", detailDTO.getMuni() == null ? JSONObject.NULL : detailDTO.getMuni());
                    detailJson.put("noExt", detailDTO.getNoExt() == null ? JSONObject.NULL : detailDTO.getNoExt());
                    detailJson.put("noInt", detailDTO.getNoInt() == null ? JSONObject.NULL : detailDTO.getNoInt());
                    detailJson.put("nomb", detailDTO.getNomb() == null ? JSONObject.NULL : detailDTO.getNomb());
                    detailJson.put("ocr", detailDTO.getOcr() == null ? JSONObject.NULL : detailDTO.getOcr());
                    detailJson.put("secc", detailDTO.getSecc() == null ? JSONObject.NULL : detailDTO.getSecc());
                    detailJson.put("vige", detailDTO.getVige());
                    detailResult = detailJson.toString();
                }
            } catch (Exception e) {
                log.error("Error finding detail for: {} with message: {}", dto.getOperationId(), e.getMessage());
            }
            StringBuilder stringBuilder = new StringBuilder("Se ha creado un nuevo expediente en TAS").append("|").append(responseEntity.getBody());
            if (detailResult != null) {
                stringBuilder.append("|").append(detailResult);
            }
            operationResult.setErrorMessage(stringBuilder.toString());
            return new ResponseEntity<>(operationResult, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Erorr uploading credentials in async mode with message: {}", e.getMessage());
            operationResult.setResultOK(false);
            operationResult.setErrorMessage("10001");
            return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Stores and validates the credentials sent as attachment list, also expects a JSON like {'operationId' : 1}", response = OperationResult.class)
    @RequestMapping(value = "/credential", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<OperationResult> credentialsOperation(
            @RequestPart(value = "json") String credentialsOperationRequest,
            @RequestPart(value = "file", required = false) List<MultipartFile> files) {
    	//log.info("lblancas: "+this.getClass().getName()+".{credentialsOperation() }");
    	log.info("EnrollmentCredentialsController.public ResponseEntity<OperationResult> credentialsOperation");
        OperationResult operationResult = new OperationResult();
        JSONObject credentialsOperationRequestJSON = null;
        if (!JsonValidation.validateJson(credentialsOperationRequest)) {
            operationResult.setErrorMessage("Bad Request");
            return new ResponseEntity<>(operationResult, HttpStatus.BAD_REQUEST);
        }
        credentialsOperationRequestJSON = new JSONObject(credentialsOperationRequest);
        Long operationId = credentialsOperationRequestJSON.getLong("operationId");
        try {
            //ResponseEntity<String> responseEntity = identificationAttachmentsClient.uploadIdentification(files, operationId);
            ResponseEntity<String> responseEntity = invokeCredentialsOperation(files, operationId); 
            if (responseEntity == null) {
                operationResult.setResultOK(false);
                operationResult.setErrorMessage("10001");
                return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
            }else if(responseEntity.getStatusCode().equals(HttpStatus.GATEWAY_TIMEOUT)){
                operationResult.setErrorMessage("TIME_OUT");
                operationResult.setResultOK(false);
                return new ResponseEntity<>(operationResult, HttpStatus.GATEWAY_TIMEOUT);
            } else if (responseEntity.getStatusCode().is4xxClientError() || responseEntity.getStatusCode().is5xxServerError()) {
                operationResult.setResultOK(false);
                operationResult.setErrorMessage(responseEntity.getBody());
                return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
            } else {
                operationResult.setResultOK(true);
                String detailResult = null;
                try {
                    ResponseEntity<IneDetailDTO> responseEntityDetail = identificationClient.findDetail(operationId);
                    if (responseEntityDetail != null && responseEntityDetail.getStatusCode().is2xxSuccessful()) {
                        IneDetailDTO detailDTO = responseEntityDetail.getBody();
                        JSONObject detailJson = new JSONObject();
                        detailJson.put("apeMat", detailDTO.getApeMat() == null ? JSONObject.NULL : detailDTO.getApeMat());
                        detailJson.put("apePat", detailDTO.getApePat() == null ? JSONObject.NULL : detailDTO.getApePat());
                        detailJson.put("call", detailDTO.getCall() == null ? JSONObject.NULL : detailDTO.getCall());
                        detailJson.put("clavElec", detailDTO.getClavElec() == null ? JSONObject.NULL : detailDTO.getClavElec());
                        detailJson.put("col", detailDTO.getCol() == null ? JSONObject.NULL : detailDTO.getCol());
                        detailJson.put("cp", detailDTO.getCp() == null ? JSONObject.NULL : detailDTO.getCp());
                        detailJson.put("dist", detailDTO.getDist() == null ? JSONObject.NULL : detailDTO.getDist());
                        detailJson.put("esta", detailDTO.getEsta() == null ? JSONObject.NULL : detailDTO.getEsta());
                        detailJson.put("foli", detailDTO.getFoli() == null ? JSONObject.NULL : detailDTO.getFoli());
                        detailJson.put("loca", detailDTO.getLoca() == null ? JSONObject.NULL : detailDTO.getLoca());
                        detailJson.put("mrz", detailDTO.getMrz() == null ? JSONObject.NULL : detailDTO.getMrz());
                        detailJson.put("muni", detailDTO.getMuni() == null ? JSONObject.NULL : detailDTO.getMuni());
                        detailJson.put("noExt", detailDTO.getNoExt() == null ? JSONObject.NULL : detailDTO.getNoExt());
                        detailJson.put("noInt", detailDTO.getNoInt() == null ? JSONObject.NULL : detailDTO.getNoInt());
                        detailJson.put("nomb", detailDTO.getNomb() == null ? JSONObject.NULL : detailDTO.getNomb());
                        detailJson.put("ocr", detailDTO.getOcr() == null ? JSONObject.NULL : detailDTO.getOcr());
                        detailJson.put("secc", detailDTO.getSecc() == null ? JSONObject.NULL : detailDTO.getSecc());
                        detailJson.put("vige", detailDTO.getVige());
                        detailResult = detailJson.toString();
                    }
                } catch (Exception e) {
                    log.error("Error finding detail for: {} with message: {}", operationId, e.getMessage());
                }
                StringBuilder stringBuilder = new StringBuilder("Se ha creado un nuevo expediente en TAS").append("|").append(responseEntity.getBody());
                if (detailResult != null) {
                    stringBuilder.append("|").append(detailResult);
                }
                operationResult.setErrorMessage(stringBuilder.toString());
                return new ResponseEntity<>(operationResult, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("Error in credentialOperation for: {} with message: {}", operationId, e.getMessage());
            operationResult.setResultOK(false);
            operationResult.setErrorMessage("10001");
            return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    
    
    
    private ResponseEntity<String> invokeCredentialsOperation(List<MultipartFile> files, Long operationId){
    	//log.info("lblancas: "+this.getClass().getName()+".{invokeCredentialsOperation() }");
    	////log.info("lblancas....private ResponseEntity<String> invokeCredentialsOperation");
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Future<ResponseEntity<String>> task = executorService.submit(()-> identificationAttachmentsClient.uploadIdentification(files, operationId));
        try{
            return task.get(100, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException e) {
            return null;
        } catch (TimeoutException e) {
            return new ResponseEntity<>("TIME_OUT", HttpStatus.GATEWAY_TIMEOUT);
        }
    }

    @RequestMapping(value = "/credential/primaryIdRollback", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public BasicRequestDTO rollbackPrimaryIdConfirmation(@RequestBody BasicRequestDTO basicRequestDTO){
    	//log.info("lblancas: "+this.getClass().getName()+".{rollbackPrimaryIdConfirmation() }");
        identificationClient.rollbackConfirmationCredentialCapture(basicRequestDTO);
        return basicRequestDTO;
    }

}

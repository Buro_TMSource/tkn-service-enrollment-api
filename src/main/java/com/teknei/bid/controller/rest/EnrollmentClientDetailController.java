package com.teknei.bid.controller.rest;

import com.teknei.bid.dto.*;
import com.teknei.bid.service.remote.CustomerClient;
import feign.FeignException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/rest/v3/enrollment/client/detail")
@CrossOrigin
public class EnrollmentClientDetailController {

    private static Logger log = LoggerFactory.getLogger(EnrollmentClientDetailController.class);
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
    @Autowired
    private CustomerClient customerClient;
    
    @ApiOperation(value = "Verifies if the customer has any previous record associated", notes = "The response should contain only one record in the array", 
    		response = BidServiceConfiguration.class)
    @RequestMapping(value = "/serviceConfiguration", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, 
    	produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<BidServiceConfiguration> getServiceConfiguration() 
    {
    	//log.info("lblancas:[2] "+this.getClass().getName()+".{getServiceConfiguration() }");
    	ResponseEntity<BidServiceConfiguration> responseEntity =customerClient.findServices("Activo"); 
    	return responseEntity;
    }
    @ApiOperation(value = "Verifies if the customer has any previous record associated", notes = "The response should contain only one record in the array", 
    		response = BidTipoClienteDTO.class)
    @RequestMapping(value = "/catalogoTipoCliente", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, 
    	produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<BidTipoClienteDTO>> getCatalogoTipoCliente() 
    {
    	log.info("lbl:"+this.getClass().getName()+".{getCatalogoTipoCliente() }");
    	ResponseEntity<List<BidTipoClienteDTO>> responseEntity =customerClient.getCatalogTipoCliente(); 
    	return responseEntity;
    }
    
    @ApiOperation(value = "Verifies if the customer has any previous record associated", notes = "The response should contain only one record in the array", response = BidIfeValidationDTO.class)
    @RequestMapping(value = "/validate/information/ine", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<BidIfeValidationDTO>> findValidation(@RequestBody BidIfeValidationRequestDTO validationRequestDTO) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findValidation() }");
        try {
            ResponseEntity<List<BidIfeValidationDTO>> responseEntity = customerClient.findValidation(validationRequestDTO);
            List<BidIfeValidationDTO> list = responseEntity.getBody();
            if (list.size() > 1) {
                return new ResponseEntity<>(list, HttpStatus.CONFLICT);
            }
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (FeignException e) {
            log.error("Error finding previous information for: {} with message : {}", validationRequestDTO, e.getMessage());
            if (e.status() == 404) {
                return new ResponseEntity<>((List<BidIfeValidationDTO>) null, HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>((List<BidIfeValidationDTO>) null, HttpStatus.BAD_REQUEST);
            }
        }
    }

    @ApiOperation(value = "Verifies if the customer has the biometric step completed", notes = "Returns 200 if ok, 404 if not found", response = Boolean.class)
    @RequestMapping(value = "/verify/biometric/{operationId}", method = RequestMethod.GET)
    public ResponseEntity<Boolean> findVerificationForCandidate(@PathVariable Long operationId) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findVerificationForCandidate() }");
        try {
            ResponseEntity<Boolean> responseEntity = customerClient.findVerificationForCandidate(operationId);
            if (responseEntity.getBody() == true) {
                return new ResponseEntity<>(true, HttpStatus.OK);
            }
            return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);
        } catch (FeignException e) {
            if (e.status() == 404) {
                return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);
            }
            log.error("Error found verification for biometric with id: {} and message: {}", operationId, e.getMessage());
            return new ResponseEntity<>(false, HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            log.error("Error found verification for biometric with id: {} and message: {}", operationId, e.getMessage());
            return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);
        }
    }


    @ApiOperation(value = "Finds the current processes opened for today", response = List.class)
    @RequestMapping(value = "/search/openedForToday", method = RequestMethod.GET)
    public ResponseEntity<List<ClientBiometricCandidateDTO>> findOpenedForToday() {
    	//log.info("lblancas: "+this.getClass().getName()+".{findOpenedForToday() }");
        try {
            ResponseEntity<List<CurpDTO>> listCandidates = customerClient.findOpenProcess();
            if (listCandidates.getStatusCode().is2xxSuccessful()) {
                List<ClientBiometricCandidateDTO> listCandidatesReturn = new ArrayList<>();
                listCandidates.getBody().forEach(c -> listCandidatesReturn.add(new ClientBiometricCandidateDTO(c.getId(), c.getCurp(), c.getScanId(), c.getDocumentId(),
                        new StringBuilder("/rest/v3/enrollment/pictures/search/customer/image/1/").append(c.getCurp()).append("/").append(c.getId()).toString(), new StringBuilder("/rest/v3/enrollment/pictures/search/customer/image/2/").append(c.getCurp()).append("/").append(c.getId()).toString(), new StringBuilder("/rest/v3/enrollment/pictures/search/customer/image/3/").append(c.getCurp()).append("/").append(c.getId()).toString())))
                ;
                return new ResponseEntity<>(listCandidatesReturn, HttpStatus.OK);
            } else {
                return new ResponseEntity<>((List<ClientBiometricCandidateDTO>) null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            log.error("Error finding opened for today with message: {}", e.getMessage());
            return new ResponseEntity<>((List<ClientBiometricCandidateDTO>) null, HttpStatus.NOT_FOUND);
        }
    }

    @ApiOperation(value = "Finds the current processes opened for contract today", response = List.class)
    @RequestMapping(value = "/search/contractsForToday", method = RequestMethod.GET)
    public ResponseEntity<List<ClientBiometricCandidateDTO>> findOpenForContractProcess() {
    	//log.info("lblancas: "+this.getClass().getName()+".{findOpenForContractProcess() }");
        try {
            ResponseEntity<List<CurpDTO>> listCandidates = customerClient.findOpenForContractProcess();
            if (listCandidates.getStatusCode().is2xxSuccessful()) {
                List<ClientBiometricCandidateDTO> listCandidatesReturn = new ArrayList<>();
                listCandidates.getBody().forEach(c -> listCandidatesReturn.add(new ClientBiometricCandidateDTO(c.getId(), c.getCurp(), c.getScanId(), c.getDocumentId(),
                        new StringBuilder("/rest/v3/enrollment/pictures/search/customer/image/1/").append(c.getCurp()).append("/").append(c.getId()).toString(), new StringBuilder("/rest/v3/enrollment/pictures/search/customer/image/2/").append(c.getCurp()).append("/").append(c.getId()).toString(), new StringBuilder("/rest/v3/enrollment/pictures/search/customer/image/3/").append(c.getCurp()).append("/").append(c.getId()).toString())))
                ;
                return new ResponseEntity<>(listCandidatesReturn, HttpStatus.OK);
            } else {
                return new ResponseEntity<>((List<ClientBiometricCandidateDTO>) null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            log.error("Error finding opened for today with message: {}", e.getMessage());
            return new ResponseEntity<>((List<ClientBiometricCandidateDTO>) null, HttpStatus.NOT_FOUND);
        }
    }

    @ApiOperation(value = "Gets the data in detail from the search parameters send as JSON. The response will be a JSON string as {nombre, apellidoPaterno, apellidoMaterno, curpCapturado, curpIdentificado, direccion, scanId, documentManagerId, mrz, ocr, vig}", response = String.class)
    @RequestMapping(value = "/detail", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public ResponseEntity<String> findDetail(@RequestBody String searchDTO) {
    	log.info("lblancas: "+this.getClass().getName()+".{findDetail("+searchDTO+") }");
        try {
            JSONObject jsonInput = new JSONObject(searchDTO);
            String curp = jsonInput.optString("curp", null);
            String mail = jsonInput.optString("mail", null);
            String tel = jsonInput.optString("tel", null);
            ResponseEntity<String> responseEntity = null;
            if (curp != null && !curp.isEmpty()) 
            {
                responseEntity = findDetailFromCurp(curp, null);
                if (responseEntity != null && responseEntity.getStatusCode().is2xxSuccessful()) 
                {
                    responseEntity = validateAdditionals(responseEntity, null, tel, mail);
                }
            } 
            else if (mail != null && !mail.isEmpty()) 
            {
                responseEntity = findDetailByMail(mail);
                if (responseEntity != null && responseEntity.getStatusCode().is2xxSuccessful()) 
                {
                    responseEntity = validateAdditionals(responseEntity, curp, tel, null);
                }
            }
            else if (tel != null && !tel.isEmpty()) 
            {
                responseEntity = findDetailByCel(tel);
                if (responseEntity != null && responseEntity.getStatusCode().is2xxSuccessful()) 
                {
                    responseEntity = validateAdditionals(responseEntity, curp, null, mail);
                }
            }
            return responseEntity;
        } catch (Exception e) {
            log.error("Error in request of findDetail with data: {} and message: {}", searchDTO, e.getMessage());
            return new ResponseEntity<>(searchDTO, HttpStatus.NOT_FOUND);
        }
    }
    @ApiOperation(value = "Gets the data in detail from the search parameters send as JSON. The response will be a JSON string as {nombre, apellidoPaterno, apellidoMaterno, curpCapturado, curpIdentificado, direccion, scanId, documentManagerId, mrz, ocr, vig}", response = String.class)
    @RequestMapping(value = "/detailByCurpAndType", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public ResponseEntity<String> findDetailbyCrpAndTp(@RequestBody String searchDTO) {
    	log.info("lblancas: "+this.getClass().getName()+".{detailByCurpAndType("+searchDTO+") }");
        try {
            JSONObject jsonInput = new JSONObject(searchDTO);
            String curp = jsonInput.optString("curp", null);
            ResponseEntity<String> responseEntity = null;
            if (curp != null && !curp.isEmpty()) 
            {
                responseEntity = findDetailFromCurpOnly(curp); 
            } 
            return responseEntity;
        } catch (Exception e) {
            log.error("Error in request of findDetail with data: {} and message: {}", searchDTO, e.getMessage());
            return new ResponseEntity<>(searchDTO, HttpStatus.NOT_FOUND);
        }
    }

    private ResponseEntity<String> validateAdditionals(ResponseEntity<String> responseEntity, String curp, String tel, String mail) {
    	//log.info("lblancas: "+this.getClass().getName()+".{validateAdditionals() }");
        /*
        if(mail != null && !mail.isEmpty()){
            log.info("Validate mail not null and not empty_: {}", mail);
            mail = Base64Utils.encodeToString(mail.getBytes());
            log.info("Validate mail not null and not empty and encoded: {}", mail);
        }
        */
        log.info("Enter into validate additinonals: {}", responseEntity);
        log.info("Params: {} {} {}" , curp, tel, mail);
        responseEntity = validateCurp(responseEntity, curp);
        log.info("Validate status 1: {}", responseEntity);
        responseEntity = validateMail(responseEntity, mail);
        log.info("Validate status 2: {}", responseEntity);
        responseEntity = validateTel(responseEntity, tel);
        log.info("Validate status 3: {}", responseEntity);
        return responseEntity;
    }

    private ResponseEntity<String> validateCurp(ResponseEntity<String> responseEntity, String curp) {
    	//log.info("lblancas: "+this.getClass().getName()+".{validateCurp() }");
        if (responseEntity != null && responseEntity.getStatusCode().is2xxSuccessful()) {
            JSONObject response = new JSONObject(responseEntity.getBody());
            if (curp != null && !curp.isEmpty()) {
                String curpFound = response.optString("curp", null);
                if (!curp.equals(curpFound)) {
                    responseEntity = new ResponseEntity<>("", HttpStatus.NOT_FOUND);
                }
            }
        }
        return responseEntity;
    }

    private ResponseEntity<String> validateTel(ResponseEntity<String> responseEntity, String tel) {
    	//log.info("lblancas: "+this.getClass().getName()+".{validateTel() }");
        if (responseEntity != null && responseEntity.getStatusCode().is2xxSuccessful()) {
            boolean found = false;
            JSONObject response = new JSONObject(responseEntity.getBody());
            if (tel != null && !tel.isEmpty()) {
                JSONArray telsFound = response.optJSONArray("tels");
                for (int i = 0; i < telsFound.length(); i++) {
                    JSONObject inner = telsFound.getJSONObject(i);
                    String telInner = inner.optString("tel", null);
                    if (telInner.equals(tel)) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    responseEntity = new ResponseEntity<>("", HttpStatus.NOT_FOUND);
                }
            }
        }
        return responseEntity;
    }

    private ResponseEntity<String> validateMail(ResponseEntity<String> responseEntity, String mail) {
    	//log.info("lblancas: "+this.getClass().getName()+".{validateMail() }");
        log.info("Validate mail");
        if (responseEntity != null && responseEntity.getStatusCode().is2xxSuccessful()) {
            JSONObject response = new JSONObject(responseEntity.getBody());
            if (mail != null && !mail.isEmpty()) {
                String mailFound = response.optString("email", null);
                if (!mail.equals(mailFound)) {
                    log.info("Not equals: {} {}", mail, mailFound);
                    responseEntity = new ResponseEntity<>("", HttpStatus.NOT_FOUND);
                }
            }
        }
        return responseEntity;
    }

    @ApiOperation(value = "Finds the current step for the requested operation")
    @RequestMapping(value = "/step", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public ResponseEntity<String> findStep(@RequestBody SearchDTO searchDTO) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findStep() }");
        try {
            ResponseEntity<Integer> integerResponseEntity = customerClient.findStepFromReferenceCurp(searchDTO.getCurp());
            ResponseEntity<ClientDetailDTO> clientDetailDTOResponseEntity = customerClient.findDetailCurp(searchDTO.getCurp());
            if (integerResponseEntity == null || clientDetailDTOResponseEntity == null || integerResponseEntity.getStatusCode().is4xxClientError() || integerResponseEntity.getStatusCode().is5xxServerError() || clientDetailDTOResponseEntity.getStatusCode().is5xxServerError() || clientDetailDTOResponseEntity.getStatusCode().is4xxClientError()) {
                throw new IllegalArgumentException();
            }
            return new ResponseEntity<>(formatOutputStep(clientDetailDTOResponseEntity.getBody(), integerResponseEntity.getBody()), HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error for: {} with message: {}", searchDTO, e.getMessage());
            return new ResponseEntity<>(formatOutputStep(null, null), HttpStatus.OK);
        }
    }


    @ApiOperation(value = "Search the customer related for the curp. The answer will be like {nombre, curp, apellidoPaterno, apellidoMaterno, email, fecha, id}")
    @RequestMapping(value = "/search/customer/{curp}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> searchByData(@PathVariable String curp) {
    	//log.info("lblancas: "+this.getClass().getName()+".{searchByData() }");
        return findDetailFromCurp(curp, null);
    }

    @ApiOperation(value = "Search the customer related for the id. The answer will be like {nombre, curp, apellidoPaterno, apellidoMaterno, email, fecha, id}")
    @RequestMapping(value = "/search/customerId/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> searchByDataId(@PathVariable Long id) {
    	//log.info("lblancas: "+this.getClass().getName()+".{searchByDataId() }");
        return findDetailFromCurp(null, id);
    }

    @ApiOperation(value = "Search the related Timestamps values for the requested CURP", response = DetailTSRecordResponseDTO.class)
    @RequestMapping(value = "/search/customer/ts/{operationId}/{curp}", method = RequestMethod.GET)
    public ResponseEntity<DetailTSRecordResponseDTO> findDetailTS(@PathVariable String operationId, @PathVariable String curp) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findDetailTS() }");
        try {
            ResponseEntity<DetailTSRecordDTO> recordDTOResponseEntity = customerClient.findDetailTSCurp(curp);
            if (recordDTOResponseEntity == null || recordDTOResponseEntity.getStatusCode().is4xxClientError() || recordDTOResponseEntity.getStatusCode().is5xxServerError()) {
                throw new IllegalArgumentException();
            }
            return new ResponseEntity<>(formatResponseDetailTS(recordDTOResponseEntity.getBody()), HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error in findDetailTS for: {} , {} with message: {}", operationId, curp, e.getMessage());
            return new ResponseEntity<>((DetailTSRecordResponseDTO) null, HttpStatus.NOT_FOUND);
        }
    }

    @ApiOperation(value = "Updates the customer record based on requested values. The id must be a valid identifier", notes = "If error ocurrs, the detail of the error will be a json in 'obs' field inside the response", response = ClientDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the customer could be updated"),
            @ApiResponse(code = 422, message = "If the customer fails in the update process"),
            @ApiResponse(code = 400, message = "Bad request, usually 'id' is missing")
    })
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public ResponseEntity<ClientDTO> updateClient(@RequestBody ClientDTO clientDTO) {
    	//log.info("lblancas: "+this.getClass().getName()+".{updateClient() }");
        try {
            return customerClient.updateClient(clientDTO);
        } catch (FeignException fe) {
            switch (fe.status()) {
                case 400:
                    return new ResponseEntity<>(clientDTO, HttpStatus.NOT_FOUND);
                case 422:
                    return new ResponseEntity<>(clientDTO, HttpStatus.UNPROCESSABLE_ENTITY);
                default:
                    return new ResponseEntity<>(clientDTO, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            log.error("Error in updateClient for : {} with message: {}", clientDTO, e.getMessage());
            return new ResponseEntity<>(clientDTO, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    private DetailTSRecordResponseDTO formatResponseDetailTS(DetailTSRecordDTO dto) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
        DetailTSRecordResponseDTO responseDTO = new DetailTSRecordResponseDTO();
        responseDTO.setAddress(dto.getAddress());
        if (dto.getAddress() != null) {
            responseDTO.setAddressStr(sdf.format(dto.getAddress()));
        } else {
            responseDTO.setAddressStr("");
        }
        responseDTO.setContract(dto.getContract());
        if (dto.getContract() != null) {
            responseDTO.setContractStr(sdf.format(dto.getContract()));
        } else {
            responseDTO.setContractStr("");
        }
        responseDTO.setCredentials(dto.getCredentials());
        if (dto.getCredentials() != null) {
            responseDTO.setCredentialsStr(sdf.format(dto.getCredentials()));
        } else {
            responseDTO.setCredentialsStr("");
        }
        responseDTO.setCurp(dto.getCurp());
        responseDTO.setFacial(dto.getFacial());
        if (dto.getFacial() != null) {
            responseDTO.setFacialStr(sdf.format(dto.getFacial()));
        } else {
            responseDTO.setFacialStr("");
        }
        responseDTO.setFingers(dto.getFingers());
        if (dto.getFingers() != null) {
            responseDTO.setFingersStr(sdf.format(dto.getFingers()));
        } else {
            responseDTO.setFingersStr("");
        }
        responseDTO.setId(dto.getId());
        return responseDTO;
    }

    private ResponseEntity<String> findDetailByMail(String mail) 
    {
    	//log.info("lblancas: "+this.getClass().getName()+".{findDetailByMail() }");
        try {
            String mailB64 = Base64Utils.encodeToString(mail.getBytes());
            ResponseEntity<ClientDetailDTO> responseEntity = customerClient.findDetaiMail(mailB64);
            if (responseEntity == null) {
                throw new IllegalArgumentException();
            } else if (responseEntity.getStatusCode().is5xxServerError() || responseEntity.getStatusCode().is4xxClientError()) {
                return new ResponseEntity<>("Error", HttpStatus.NOT_FOUND);
            }
            String output = formatOutputDetail(responseEntity.getBody());
            return new ResponseEntity<>(output, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error in searchByData for: {} with message: {}", mail, e.getMessage());
            return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
        }
    }

    private ResponseEntity<String> findDetailByTel(String tel) 
    {
    	//log.info("lblancas: "+this.getClass().getName()+".{findDetailByTel() }");
        try {
            ResponseEntity<ClientDetailDTO> responseEntity = customerClient.findDetailTel(tel);
            if (responseEntity == null) {
                throw new IllegalArgumentException();
            } else if (responseEntity.getStatusCode().is5xxServerError() || responseEntity.getStatusCode().is4xxClientError()) {
                return new ResponseEntity<>("Error", HttpStatus.NOT_FOUND);
            }
            String output = formatOutputDetail(responseEntity.getBody());
            return new ResponseEntity<>(output, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error in searchByData for: {} with message: {}", tel, e.getMessage());
            return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
        }
    }

    private ResponseEntity<String> findDetailByCel(String tel) 
    {
    	log.info("lblancas: "+this.getClass().getName()+".{findDetailByCel("+tel+") }");
        try {
            ResponseEntity<ClientDetailDTO> responseEntity = customerClient.findDetailTel(tel); 
            if (responseEntity == null) {
                throw new IllegalArgumentException();
            } else if (responseEntity.getStatusCode().is5xxServerError() || responseEntity.getStatusCode().is4xxClientError()) {
                return new ResponseEntity<>("Error", HttpStatus.NOT_FOUND);
            }
            String output = formatOutputDetail(responseEntity.getBody());
            return new ResponseEntity<>(output, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error in searchByData for: {} with message: {}", tel, e.getMessage());
            return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
        }
    }

    private ResponseEntity<String> findDetailFromCurp(String curp, Long id) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findDetailFromCurp() }");
        try {
            ResponseEntity<ClientDetailDTO> responseEntity = null;
            if (id == null) {
                responseEntity = customerClient.findDetailCurp(curp);
            } else {
                responseEntity = customerClient.findDetailId(id);
            }
            if (responseEntity == null) {
                throw new IllegalArgumentException();
            } else if (responseEntity.getStatusCode().is5xxServerError() || responseEntity.getStatusCode().is4xxClientError()) {
                return new ResponseEntity<>("Error", HttpStatus.NOT_FOUND);
            }
            String output = formatOutputDetail(responseEntity.getBody());
            return new ResponseEntity<>(output, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error in searchByData for: {} with message: {}", curp, e.getMessage());
            return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
        }
    }
    private ResponseEntity<String> findDetailFromCurpOnly(String curp) {
    	log.info("lblancas: "+this.getClass().getName()+".{findDetailFromCurpOnly() }");
        try {
            ResponseEntity<ClientDetailDTO> responseEntity = null;
            responseEntity = customerClient.FindDetailByCurpAndPerfil(curp,"4");
             
            if (responseEntity == null) {
                throw new IllegalArgumentException();
            } else if (responseEntity.getStatusCode().is5xxServerError() || responseEntity.getStatusCode().is4xxClientError()) {
                return new ResponseEntity<>("Error", HttpStatus.NOT_FOUND);
            }
            String output = formatOutputDetail(responseEntity.getBody());
            return new ResponseEntity<>(output, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error in searchByData for: {} with message: {}", curp, e.getMessage());
            return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
        }
    }

    private String formatOutputStep(ClientDetailDTO dto, Integer step) {
    	//log.info("lblancas: "+this.getClass().getName()+".{formatOutputStep() }");
        JSONObject jsonObject = new JSONObject();
        if (dto == null) {
            jsonObject.put("operationId", 0);
            jsonObject.put("step", 0);
            return jsonObject.toString();
        }
        jsonObject.put("operationId", dto.getId());
        jsonObject.put("step", step);
        return jsonObject.toString();
    }

    private String formatOutputDetail(ClientDetailDTO dto) {
    	//log.info("lblancas: "+this.getClass().getName()+".{formatOutputDetail() }");
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("nombre", dto.getName());
        jsonObject.put("apellidoPaterno", dto.getSurnameFirst());
        jsonObject.put("apellidoMaterno", dto.getSurnameLast());
        jsonObject.put("curpCapturado", dto.getCurp());
        jsonObject.put("curpIdentificado", dto.getCurpDocument());
        jsonObject.put("direccion", dto.getDire());
        jsonObject.put("scanId", dto.getScanId());
        jsonObject.put("documentManagerId", dto.getDocumentId());
        jsonObject.put("mrz", dto.getMrz());
        jsonObject.put("ocr", dto.getOcr());
        jsonObject.put("vig", dto.getVig());
        jsonObject.put("id", dto.getId());
        jsonObject.put("email", dto.getEmail());
        jsonObject.put("status", "ok");
        jsonObject.put("fecha", new Date().toString());
        jsonObject.put("curp", dto.getCurp());
        jsonObject.put("curpDocument", dto.getCurpDocument());
        jsonObject.put("tels", dto.getTelephones());
        jsonObject.put("idType", dto.getIdClientType());
        return jsonObject.toString();
    }
}

package com.teknei.bid.controller.rest.util.crypto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
@Service
public class Decrypt {

	
    @Value("${tkn.crypto.url}")
    private String keyURL;
    @Value("${tkn.crypto.public.url}")
    private String publicKeyUrl;
    @Value("${tkn.crypto.log_output}")
    private String logOutput;
    private String fullLogOut;
    private byte[] key;
    private static final Logger log = LoggerFactory.getLogger(Decrypt.class);
    private Rules rules;
    private Path pathOut;
    private AtomicLong atml;

    @PostConstruct
    private void postConstruct() {
    	//log.info("lblancas: "+this.getClass().getName()+".{postConstruct() }");
        atml = new AtomicLong();
        try {
            fullLogOut = logOutput.trim().replace("#", "");
            pathOut = Paths.get(fullLogOut);
        } catch (Exception e) {
            log.error("No log-output supplied");
        }
        rules = new Rules();
        String uriKey = keyURL.trim().replace("#", "");
        try {
            String b64Key = new String(Files.readAllBytes(Paths.get(uriKey)), "utf-8");
            key = Base64Utils.decodeFromString(b64Key);
        } catch (IOException e) {
            log.error("Error reading key with message: {}", e.getMessage());
        }
    }

    public String encrypt(String b64Source){
    	//log.info("lblancas: "+this.getClass().getName()+".{encrypt() }");
        long id =  atml.incrementAndGet();
        byte[] source = Base64Utils.decodeFromString(b64Source);
        byte[] ciphered = rules.generate(source, String.valueOf(id), String.valueOf(id), "1", publicKeyUrl);
        String b64Ciphered = Base64Utils.encodeToString(ciphered);
        return b64Ciphered;
    }

    public String decrypt(String b64Source) {
    	//log.info("lblancas: "+this.getClass().getName()+".{decrypt() }");
        byte[] source = Base64Utils.decodeFromString(b64Source);
        List<byte[]> listDecoded = rules.decryptChain(source, key);
        if (listDecoded == null || listDecoded.isEmpty()) {
            return null;
        }
        byte[] originByteArray = listDecoded.get(0);
        String realSource = Base64Utils.encodeToString(originByteArray);
        appendToLog(b64Source, realSource);
        return realSource;
    }

    private void appendToLog(String cipher, String clear) {
    	//log.info("lblancas: "+this.getClass().getName()+".{appendToLog() }");
        /*String endMarker = "-------------------------------------------------------------";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Clear", clear);
        jsonObject.put("Cipher", cipher);
        try {
            Files.write(pathOut, jsonObject.toString().getBytes(), StandardOpenOption.APPEND);
            Files.write(pathOut, endMarker.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            log.error("Could not write to log with clear and cipher values: {}", e.getMessage());
        }*/
    }


}

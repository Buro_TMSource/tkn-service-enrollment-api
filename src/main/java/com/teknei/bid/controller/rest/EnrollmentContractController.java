package com.teknei.bid.controller.rest;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.teknei.bid.controller.rest.util.crypto.Decrypt;
import com.teknei.bid.controller.rest.util.crypto.TokenUtils;
import com.teknei.bid.dto.*;
import com.teknei.bid.service.remote.BiometricClient;
import com.teknei.bid.service.remote.ContractClient;
import com.teknei.bid.service.remote.impl.ContractAttachmentclient;
import feign.FeignException;
import io.swagger.annotations.ApiOperation;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.util.Base64;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/rest/v3/enrollment/contract")
@CrossOrigin
public class EnrollmentContractController {

    private static final Logger log = LoggerFactory.getLogger(EnrollmentContractController.class);
    @Autowired
    private ContractAttachmentclient contractAttachmentclient;
    @Autowired
    private ContractClient contractClient;
    @Autowired
    private BiometricClient biometricClient;
    @Autowired
    private Decrypt decrypt;
    @Autowired
    private TokenUtils tokenUtils;
    @Autowired
    private EnrollmentCertController certController;

    @ApiOperation(value = "Stores the customer credentials related to the account and the contract generation", response = Byte.class)
    @RequestMapping(value = "/credentials", method = RequestMethod.POST)
    public ResponseEntity<Boolean> addCustomerCredentials(@RequestBody CustomerCredentials credentials) {
    	//log.info("lblancas: "+this.getClass().getName()+".{addCustomerCredentials() }");
        return new ResponseEntity<>(Boolean.TRUE, HttpStatus.OK);
    }

    @ApiOperation(value = "Generates and gets the contract for the current operation", response = byte[].class)
    @RequestMapping(value = "/contrato/{id}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getUnsignedContract(@PathVariable("id") Long id, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{getUnsignedContract() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        try {
            return contractClient.getUnsignedContract(id, username);
        } catch (Exception e) {
            log.error("Error in getUnsignedContract for: {} with message: {}", id, e.getMessage());
            return new ResponseEntity<>((byte[]) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Generates, pre fills and gets the contract for the current operation", response = byte[].class)
    @RequestMapping(value = "/contractPrefilled/{id}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getContractPrefilled(@PathVariable("id") Long id, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{getContractPrefilled() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        try {
            return contractClient.getUnsignedContractWithCerts(id, username);
        } catch (Exception e) {
            log.error("Error in getUnsignedContract for: {} with message: {}", id, e.getMessage());
            return new ResponseEntity<>((byte[]) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
 
    @ApiOperation(value = "Sign the contract with the cyphered fingerprint. Verifies the identify of the customer", response = String.class)
    @RequestMapping(value = "/contrato/signCyphered", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> signContractCyphered(@RequestBody SignContractRequestDTO requestDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{signContractCyphered() }");
    	
    	//log.info("lblancas: "+this.getClass().getName()+".{signContractCyphered()  }  ["+requestDTO.toString()+"]");
    	
    	
    	
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        String b64Finger = requestDTO.getBase64Finger();
        String clearFinger = decrypt.decrypt(b64Finger);
        JSONObject requestBiom = new JSONObject();
        requestBiom.put("id", String.valueOf(requestDTO.getOperationId()));
        requestBiom.put("ri", clearFinger);
        requestBiom.put("contentType", requestDTO.getContentType());
        try {
        	//log.info("lblancas: "+this.getClass().getName()+".{signContractCyphered() }  ---  > busca biometricos  inicia ");
            biometricClient.searchByFingerAndId(requestBiom.toString());
            //log.info("lblancas: "+this.getClass().getName()+".{signContractCyphered() }  ---  > busca biometricos  Termina");

            String hash = "{\"hash\": \"temphash\"}";
            try {
                OperationIdDTO operationIdDTO = new OperationIdDTO();
                operationIdDTO.setOperationId(requestDTO.getOperationId());
                
               /**
                 * Aqui genera el certificado antes de firmar. INICIA
                 */
                
                //log.info("lblancas: "+this.getClass().getName()+".{signContractCyphered() ------------->} certController.generateCert   --  INICIA ");
                //ResponseEntity<String> responseEntity = certController.generateCert(operationIdDTO, request);
                //log.info("lblancas: "+this.getClass().getName()+".{signContractCyphered() ------------->} certController.generateCert   --  TERMINA ");
                
                //log.info("lblancas: "+this.getClass().getName()+".{signContractCyphered() ------------->} certController.generateCert   --  is2xxSuccessful "+responseEntity.getStatusCode().is2xxSuccessful());
                //log.info("lblancas: "+this.getClass().getName()+".{signContractCyphered() ------------->} certController.generateCert   --  StatusCodeValue "+responseEntity.getStatusCodeValue());
                
                //if (responseEntity.getStatusCode().is2xxSuccessful() || responseEntity.getStatusCodeValue() == 409) {
                    //return contractController.getUnsignedContract(idCustomer, request);
                    contractClient.getUnsignedContractWithCerts(requestDTO.getOperationId(), username);
                //} else {
                //    return new ResponseEntity<>((String) null, HttpStatus.FORBIDDEN);
                //}
                
                //log.info("lblancas: "+this.getClass().getName()+" Invoco la generacion de contrato sin la generacion del certificado "
               // 		+ "["+requestDTO.getOperationId()+","+username+"]");
                /**
                 * Aqui genera el certificado antes de firmar. FIN
                 */
                
                /**
                 * Genera el contrato
                 * Se omite 
                 */
                //contractClient.getUnsignedContractWithCerts(requestDTO.getOperationId(), username);
                /**
                 * Genera Contrato
                 */
            } catch (Exception e) {
                log.error("Error generating contract with cert details: {}", e.getMessage());
                return new ResponseEntity<>((String) null, HttpStatus.FORBIDDEN);
            }
            ResponseEntity<String> responseHash = biometricClient.getHashForCustomer(String.valueOf(requestDTO.getOperationId()));
            hash = responseHash.getBody();


            JSONObject jsonObject = new JSONObject(hash);
            requestDTO.setHash(jsonObject.getString("hash"));
            requestDTO.setBase64Finger(clearFinger);
            requestDTO.setUsername(username);
            contractClient.signContract(requestDTO);
            JSONObject okResponse = new JSONObject();
            okResponse.put("status", "OK");
            return new ResponseEntity<>(okResponse.toString(), HttpStatus.OK);
        } catch (FeignException e) {
            JSONObject jsonError = new JSONObject();
            switch (e.status()) {
                case 404:
                    jsonError.put("status", "404");
                    return new ResponseEntity<>(jsonError.toString(), HttpStatus.NOT_FOUND);
                case 422:
                    jsonError.put("status", "422 - unable to generate contract");
                    return new ResponseEntity<>(jsonError.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
                default:
                    jsonError.put("status", "500 - error not recognized");
                    log.error("Error in contract sign with message: {}", e.getMessage());
                    return new ResponseEntity<>(jsonError.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            log.error("Error in contract sign with message: {}", e.getMessage());
            JSONObject jsonError = new JSONObject();
            jsonError.put("status", "500");
            return new ResponseEntity<>(jsonError.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }


    @ApiOperation(value = "Sign the contract with the cyphered fingerprint. Verifies the identify of the customer", response = String.class)
    @RequestMapping(value = "/contrato/signCypheredjpg", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> signContractCypheredjpg(@RequestBody SignContractRequestDTO requestDTO, HttpServletRequest request) {
    	log.info("lblancas: "+this.getClass().getName()+".{signContractCypheredJPG() }");
        try 
        {
            contractClient.signContract(requestDTO);
            JSONObject okResponse = new JSONObject();
            okResponse.put("status", "OK");
            return new ResponseEntity<>(okResponse.toString(), HttpStatus.OK);
        } catch (FeignException e) {
            JSONObject jsonError = new JSONObject();
            switch (e.status()) {
                case 404:
                    jsonError.put("status", "404");
                    return new ResponseEntity<>(jsonError.toString(), HttpStatus.NOT_FOUND);
                case 422:
                    jsonError.put("status", "422 - unable to generate contract");
                    return new ResponseEntity<>(jsonError.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
                default:
                    jsonError.put("status", "500 - error not recognized");
                    log.error("Error in contract sign with message: {}", e.getMessage());
                    return new ResponseEntity<>(jsonError.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            log.error("Error in contract sign with message: {}", e.getMessage());
            JSONObject jsonError = new JSONObject();
            jsonError.put("status", "500");
            return new ResponseEntity<>(jsonError.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
    
    @ApiOperation(value = "Sign the contract with the cyphered fingerprint. Verifies the identify of the customer", response = String.class)
    @RequestMapping(value = "/contrato/signCypheredLABB", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<byte[]> signContractCypheredLABB(@RequestBody Long  id, String  username) {
    	//log.info("lblancas: "+this.getClass().getName()+".{signContractCyphered() }");
        return contractClient.getUnsignedContractWithCerts(id, username);
    }


    @Deprecated
    @ApiOperation(value = "Sign the contract with the fingerprint. Verifies the identify of the customer", response = String.class)
    @RequestMapping(value = "/contrato/sign", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> signContract(@RequestBody SignContractRequestDTO requestDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{signContract() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        String b64Finger = requestDTO.getBase64Finger();
        JSONObject requestBiom = new JSONObject();
        requestBiom.put("id", String.valueOf(requestDTO.getOperationId()));
        requestBiom.put("ri", b64Finger);
        requestBiom.put("contentType", requestDTO.getContentType());
        try {
            biometricClient.searchByFingerAndId(requestBiom.toString());
            try {
                OperationIdDTO operationIdDTO = new OperationIdDTO();
                operationIdDTO.setOperationId(requestDTO.getOperationId());
                ResponseEntity<String> responseEntity = certController.generateCert(operationIdDTO, request);
                if (responseEntity.getStatusCode().is2xxSuccessful() || responseEntity.getStatusCodeValue() == 409) {
                    //return contractController.getUnsignedContract(idCustomer, request);
                    contractClient.getUnsignedContractWithCerts(requestDTO.getOperationId(), username);
                } else {
                    return new ResponseEntity<>((String) null, HttpStatus.FORBIDDEN);
                }
            } catch (Exception e) {
                log.error("Error generating contract with cert details: {}", e.getMessage());
            }
            requestDTO.setUsername(username);
            ResponseEntity<String> responseHash = biometricClient.getHashForCustomer(String.valueOf(requestDTO.getOperationId()));
            String hash = responseHash.getBody();
            JSONObject jsonObject = new JSONObject(hash);
            requestDTO.setHash(jsonObject.getString("hash"));
            contractClient.signContract(requestDTO);
            JSONObject okResponse = new JSONObject();
            okResponse.put("status", "OK");
            return new ResponseEntity<>(okResponse.toString(), HttpStatus.OK);
        } catch (FeignException e) {
            JSONObject jsonError = new JSONObject();
            switch (e.status()) {
                case 404:
                    jsonError.put("status", "404");
                    return new ResponseEntity<>(jsonError.toString(), HttpStatus.NOT_FOUND);
                case 422:
                    jsonError.put("status", "422 - unable to generate contract");
                    return new ResponseEntity<>(jsonError.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
                default:
                    jsonError.put("status", "500 - error not recognized");
                    log.error("Error in contract sign with message: {}", e.getMessage());
                    return new ResponseEntity<>(jsonError.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            log.error("Error in contract sign with message: {}", e.getMessage());
            JSONObject jsonError = new JSONObject();
            jsonError.put("status", "500");
            return new ResponseEntity<>(jsonError.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Generates and gets the contract for the current operation", response = String.class)
    @RequestMapping(value = "/contratoB64/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getUnsignedContractB64(@PathVariable("id") Long id, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{getUnsignedContractB64() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        try {
            return contractClient.getUnsignedContractB64(id, username);
        } catch (Exception e) {
            log.error("Error in getUnsignedContract for: {} with message: {}", id, e.getMessage());
            return new ResponseEntity<>((String) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
    

    @ApiOperation(value = "Finds the values that must be accepted by the customer", response = AcceptancePerCustomerDTO.class)
    @RequestMapping(value = "/contrato/contratoDemo", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getContractDemo(@RequestBody ContractDemoDTO dto,HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{getContractDemo() } "+dto.toString());
        return contractClient.getDemo(dto);
    }


    @Deprecated
    @ApiOperation(value = "Adds the signed contract to the case file. It expects the file as attachment and the operationId in the URL", response = OperationResult.class)
    @RequestMapping(value = "/contrato/add/{id}", method = RequestMethod.POST)
    public ResponseEntity<OperationResult> addSignedContract(@RequestPart(value = "file") MultipartFile file, @PathVariable("id") Long id) {
    	//log.info("lblancas: "+this.getClass().getName()+".{addSignedContract() }");
        OperationResult operationResult = new OperationResult();
        try {
            ResponseEntity<String> responseEntity = contractAttachmentclient.addSignedContract(file, id);
            if (responseEntity == null) {
                throw new IllegalArgumentException();
            } else if (responseEntity.getStatusCode().is4xxClientError() || responseEntity.getStatusCode().is5xxServerError()) {
                operationResult.setErrorMessage(responseEntity.getBody());
                operationResult.setResultOK(false);
                return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
            }
            operationResult.setResultOK(true);
            operationResult.setErrorMessage(responseEntity.getBody());
            return new ResponseEntity<>(operationResult, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error in addSignedContract for: {} with message: {}", id, e.getMessage());
            operationResult.setErrorMessage("ERROR");
            operationResult.setResultOK(false);
            return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Uploads contract signed and stores in the document manager", notes = "The content should be ciphered", response = OperationResult.class)
    @RequestMapping(value = "/uploadPlain", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> addPlainSignedContract(@RequestBody ContractSignedDTO contractSignedDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{addPlainSignedContract() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        String contract = null;
        try {
            String content = decrypt.decrypt(contractSignedDTO.getFileB64());
            if (content == null) {
                contract = contractSignedDTO.getFileB64();
            } else {
                contract = content;
            }
        } catch (Exception e) {
            contract = contractSignedDTO.getFileB64();
            log.error("Error deciphering content, using plain: {}", e.getMessage());
        }
        contractSignedDTO.setFileB64(contract);
        contractSignedDTO.setUsername(username);
        OperationResult operationResult = new OperationResult();
        try {
            ResponseEntity<String> responseEntity = contractClient.addPlainSignedContract(contractSignedDTO);
            operationResult.setResultOK(true);
            operationResult.setErrorMessage(responseEntity.getBody());
            return new ResponseEntity<>(operationResult, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error in addSignedContract for: {} with message: {}", contractSignedDTO.getOperationId(), e.getMessage());
            operationResult.setErrorMessage("ERROR");
            operationResult.setResultOK(false);
            return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Finds the values that must be accepted by the customer", response = AcceptancePerCustomerDTO.class)
    @RequestMapping(value = "/acceptancePerCustomer", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AcceptancePerCustomerDTO>> getAcceptancesPerCustomer(@RequestBody OperationIdDTO operationIdDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{getAcceptancesPerCustomer() }");
        try {
            return contractClient.getAcceptancesPerCustomer(operationIdDTO);
        } catch (Exception e) {
            log.info("Error finding acceptances per customer for: {} with message: {}", operationIdDTO, e.getMessage());
            return new ResponseEntity<>((List<AcceptancePerCustomerDTO>) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Updates the acceptances of the customer", response = String.class)
    @RequestMapping(value = "/acceptancePerCustomer", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> updateAcceptancePerCustomer(@RequestBody List<AcceptancePerCustomerDTO> dtoList, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{updateAcceptancePerCustomer() }");
        try {
            String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
            dtoList.forEach(c -> c.setUsernameRequesting(username));
            return contractClient.updateAcceptancePerCustomer(dtoList);
        } catch (Exception e) {
            log.error("Error updating acceptances per customer with message: {}", e.getMessage());
            return new ResponseEntity<>((String) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
}

package com.teknei.bid.controller.rest.unauth;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.teknei.bid.controller.rest.EnrollmentMailController;
import com.teknei.bid.dto.MailRequestDTO;
import com.teknei.bid.dto.OTPVerificationDTO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/rest/unauth/enrollment/mail")
@CrossOrigin
public class EnrollmentMailControllerUnauth {
	private static final Logger log = LoggerFactory.getLogger(EnrollmentMailControllerUnauth.class);
    @Autowired
    private EnrollmentMailController controller;

    @ApiOperation(value = "Sends the generated contract for the related customer", response = String.class)
    @RequestMapping(value = "/contractSigned", method = RequestMethod.POST)
    public ResponseEntity<String> sendContract(@RequestBody MailRequestDTO mailRequestDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{sendContract(POST) }");
        return controller.sendContract(mailRequestDTO, request);
    }

    @ApiOperation(value = "Puts an OTP key in DB related to the current id for the customer. Deactivates the old ones", response = String.class)
    @RequestMapping(value = "/mail/verification/otp/resend", method = RequestMethod.POST)
    public ResponseEntity<String> reGenerateOTP(@RequestBody MailRequestDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{reGenerateOTP(POST) }");
        return controller.reGenerateOTP(dto, request);
    }

    @ApiOperation(value = "Puts an OTP key in DB related to the current id for the customer", response = String.class)
    @RequestMapping(value = "/mail/verification/otp", method = RequestMethod.POST)
    public ResponseEntity<String> generateOTP(@RequestBody MailRequestDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{generateOTP(POST) }");
        return controller.generateOTP(dto, request);
    }

    @ApiOperation(value = "Validates OTP and uses it", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Validation successfull"),
            @ApiResponse(code = 404, message = "No OTP found"),
            @ApiResponse(code = 422, message = "Error validating OTP")
    })
    @RequestMapping(value = "/validateOTP", method = RequestMethod.POST)
    public ResponseEntity<String> validateOTP(@RequestBody OTPVerificationDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{generateOTP(POST) }");
        return controller.validateOTP(dto, request);
    }

    @ApiOperation(value = "Validates current OTP for customer and triggers the certificate request")
    @RequestMapping(value = "/validateOTP/cert", method = RequestMethod.POST)
    public ResponseEntity<String> validateOtpAndGenerateCert(@RequestBody OTPVerificationDTO dto, HttpServletRequest request){
    	//log.info("lblancas: "+this.getClass().getName()+".{validateOtpAndGenerateCert(POST) }");
        return controller.validateOtpAndGenerateCert(dto, request);
    }

    @ApiOperation(value = "Generates an OTP for videoconference")
    @RequestMapping(value = "/mail/verification/otp/videoconference", method = RequestMethod.POST)
    public ResponseEntity<String> generateOTPVideoconference(@RequestBody MailRequestDTO dto, HttpServletRequest request){
    	//log.info("lblancas: "+this.getClass().getName()+".{generateOTPVideoconference(POST) }");
        return controller.generateOTPVideoconference(dto, request);
    }

}

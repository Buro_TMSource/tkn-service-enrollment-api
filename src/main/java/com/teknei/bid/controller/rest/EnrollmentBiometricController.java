package com.teknei.bid.controller.rest;

import com.teknei.bid.controller.rest.util.crypto.Decrypt;
import com.teknei.bid.controller.rest.util.crypto.TokenUtils;
import com.teknei.bid.dto.*;
import com.teknei.bid.service.remote.BiometricClient;
import com.teknei.bid.service.remote.FacialClient;
import com.teknei.bid.service.remote.IdentificationClient;
import feign.FeignException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import static com.teknei.bid.service.validation.JsonValidation.validateJson;

@RestController
@RequestMapping(value = "/rest/v3/enrollment/biometric")
@CrossOrigin
public class EnrollmentBiometricController {

    @Autowired
    private BiometricClient biometricClient;
    @Autowired
    private FacialClient facialClient;
    @Autowired
    private IdentificationClient identificationClient;
    @Autowired
    private Decrypt decrypt;
    @Autowired
    private TokenUtils tokenUtils;
    private static final String[] FIELDS_ALL_FINGERS = {"ll", "lr", "lm", "li", "lt", "rl", "rr", "rm", "ri", "rt"};
    private static final String[] FIELDS_SLAPS = {"ts", "rs", "ls"};
    private static final String[] FIELDS_FACE = {"facial"};
    private static final Logger log = LoggerFactory.getLogger(EnrollmentBiometricController.class);


    @ApiOperation(value = "Adds the binary information from the cyphered capture of the slaps. Expects a JSON like {'operationId' : 1, 'scanId' : 'currentScan' , 'documentId' : 'currentDocumentManagerId' , 'rs' : 'Right slap' , 'ls : 'left slap' , 'ts' : 'thumbs slap'")
    @RequestMapping(value = "/minuciasSlapsCyphered", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public ResponseEntity<OperationResult> addMinuciasSlapsCyphered(@RequestBody String jsonRequest, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{addMinuciasSlapsCyphered() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        OperationResult operationResult = new OperationResult();
        JSONObject operationRequestJSON = null;
        if (!validateJson(jsonRequest)) {
            operationResult.setErrorMessage("Bad Request");
            return new ResponseEntity<>(operationResult, HttpStatus.BAD_REQUEST);
        }
        operationRequestJSON = new JSONObject(jsonRequest);
        String type = operationRequestJSON.optString("type", "SLAPS");
        Long operationId = operationRequestJSON.getLong("operationId");
        operationRequestJSON = updateJsonRequest(operationRequestJSON, FIELDS_SLAPS);
        operationRequestJSON.put("username", username);
        try {
            String jsonToSend = updateRequest(operationRequestJSON.toString());
            ResponseEntity<String> responseEntity = biometricClient.addMinucias(jsonToSend, operationId, 2);
            if (responseEntity == null) {
                throw new IllegalArgumentException();
            } else if (responseEntity.getStatusCode().is4xxClientError()) {
                operationResult.setResultOK(false);
                operationResult.setErrorMessage(responseEntity.getBody());
                return new ResponseEntity<>(operationResult, responseEntity.getStatusCode());
            } else if (responseEntity.getStatusCode().is5xxServerError()) {
                operationResult.setResultOK(false);
                operationResult.setErrorMessage(responseEntity.getBody());
                return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
            }
            operationResult.setResultOK(true);
            operationResult.setErrorMessage("Se han almacenado correctamente los datos biometricos");
            return new ResponseEntity<>(operationResult, HttpStatus.OK);
        } catch (FeignException fe) {
            ResponseEntity<OperationResult> resultResponseEntity = parseFeign(fe);
            if (resultResponseEntity.getBody().getErrorMessage().equals("40003")) {
                ResponseEntity<String> responseEntity = null;
                try {
                    responseEntity = biometricClient.searchBySlaps(operationRequestJSON.toString());
                    String bodyString = responseEntity.getBody();
                    JSONObject jsonObject = new JSONObject(bodyString);
                    String id = jsonObject.getString("id");
                    OperationResult operationResult1 = new OperationResult();
                    operationResult1.setResultOK(false);
                    JSONObject operationJsonResult = new JSONObject();
                    operationJsonResult.put("id", Long.valueOf(id));
                    operationJsonResult.put("status", "40003");
                    operationResult1.setErrorMessage(operationJsonResult.toString());
                    return new ResponseEntity<>(operationResult1, HttpStatus.UNPROCESSABLE_ENTITY);
                } catch (Exception e) {
                    log.error("Duplicated record, unable to find related one with message: {}", e.getMessage());
                    return resultResponseEntity;
                }
            } else if (resultResponseEntity.getBody().getErrorMessage().equals("40006")) {
                return new ResponseEntity<>(resultResponseEntity.getBody(), HttpStatus.PRECONDITION_FAILED);
            } else {
                return resultResponseEntity;
            }
        } catch (Exception e) {
            log.error("Error in addMinuciasSlaps for: {} - {} with message: {}", operationId, type, e.getMessage());
            operationResult.setResultOK(false);
            operationResult.setErrorMessage("40002");
            return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @Deprecated
    @ApiOperation(value = "Adds the binary information from the capture of the slaps. Expects a JSON like {'operationId' : 1, 'scanId' : 'currentScan' , 'documentId' : 'currentDocumentManagerId' , 'rs' : 'Right slap' , 'ls : 'left slap' , 'ts' : 'thumbs slap'")
    @RequestMapping(value = "/minuciasSlaps", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public ResponseEntity<OperationResult> addMinuciasSlaps(
            @RequestBody String jsonRequest, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{addMinuciasSlaps() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        OperationResult operationResult = new OperationResult();
        JSONObject operationRequestJSON = null;
        if (!validateJson(jsonRequest)) {
            operationResult.setErrorMessage("Bad Request");
            return new ResponseEntity<>(operationResult, HttpStatus.BAD_REQUEST);
        }
        operationRequestJSON = new JSONObject(jsonRequest);
        String type = operationRequestJSON.optString("type", "SLAPS");
        Long operationId = operationRequestJSON.getLong("operationId");
        try {
            String jsonToSend = updateRequest(jsonRequest);
            JSONObject jsonObject = new JSONObject(jsonToSend);
            jsonObject.put("username", username);
            ResponseEntity<String> responseEntity = biometricClient.addMinucias(jsonObject.toString(), operationId, 2);
            if (responseEntity == null) {
                throw new IllegalArgumentException();
            } else if (responseEntity.getStatusCode().is4xxClientError()) {
                operationResult.setResultOK(false);
                operationResult.setErrorMessage(responseEntity.getBody());
                return new ResponseEntity<>(operationResult, responseEntity.getStatusCode());
            } else if (responseEntity.getStatusCode().is5xxServerError()) {
                operationResult.setResultOK(false);
                operationResult.setErrorMessage(responseEntity.getBody());
                return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
            }
            operationResult.setResultOK(true);
            operationResult.setErrorMessage("Se han almacenado correctamente los datos biometricos");
            return new ResponseEntity<>(operationResult, HttpStatus.OK);
        } catch (FeignException fe) {
            ResponseEntity<OperationResult> resultResponseEntity = parseFeign(fe);
            if (resultResponseEntity.getBody().getErrorMessage().equals("40003")) {
                ResponseEntity<String> responseEntity = null;
                try {
                    responseEntity = biometricClient.searchBySlaps(jsonRequest);
                    String bodyString = responseEntity.getBody();
                    JSONObject jsonObject = new JSONObject(bodyString);
                    String id = jsonObject.getString("id");
                    OperationResult operationResult1 = new OperationResult();
                    operationResult1.setResultOK(false);
                    JSONObject operationJsonResult = new JSONObject();
                    operationJsonResult.put("id", Long.valueOf(id));
                    operationJsonResult.put("status", "40003");
                    operationResult1.setErrorMessage(operationJsonResult.toString());
                    return new ResponseEntity<>(operationResult1, HttpStatus.UNPROCESSABLE_ENTITY);
                } catch (Exception e) {
                    log.error("Duplicated record, unable to find related one with message: {}", e.getMessage());
                    return resultResponseEntity;
                }
            } else if (resultResponseEntity.getBody().getErrorMessage().equals("40006")) {
                return new ResponseEntity<>(resultResponseEntity.getBody(), HttpStatus.PRECONDITION_FAILED);
            } else {
                return resultResponseEntity;
            }
        } catch (Exception e) {
            log.error("Error in addMinuciasSlaps for: {} - {} with message: {}", operationId, type, e.getMessage());
            operationResult.setResultOK(false);
            operationResult.setErrorMessage("40002");
            return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Adds the cyphered binary information from the capture of the fingers. Expects a JSON like {'operationId' : 1, 'scanId' : 'currentScan' , 'documentId' : 'currentDocumentManagerId' , 'll' : 'x' , 'lr' : 'x', 'lm' : 'x' , 'li': 'x', 'lt' : 'x', 'rl' : 'x', 'rr' : 'x', 'rm' : 'x', 'ri' : 'x'} as first 'l' stands for left and  'r' for right and second letter stands for little, ring, middle, index and thumb")
    @RequestMapping(value = "/minuciasCyphered", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public ResponseEntity<OperationResult> addMinuciasCyphered(
            @RequestPart(value = "json") String jsonRequest, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{addMinuciasCyphered() }");
        OperationResult operationResult = new OperationResult();
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        JSONObject operationRequestJSON = null;
        if (!validateJson(jsonRequest)) {
            operationResult.setErrorMessage("Bad Request");
            return new ResponseEntity<>(operationResult, HttpStatus.BAD_REQUEST);
        }
        operationRequestJSON = new JSONObject(jsonRequest);
        String type = operationRequestJSON.optString("type", "FINGERS");
        Long operationId = operationRequestJSON.getLong("operationId");
        operationRequestJSON = updateJsonRequest(operationRequestJSON, FIELDS_ALL_FINGERS);
        try {
            String jsonToSend = updateRequest(operationRequestJSON.toString());
            JSONObject jsonObject = new JSONObject(jsonToSend);
            jsonObject.put("username", username);
            ResponseEntity<String> responseEntity = biometricClient.addMinucias(jsonObject.toString(), operationId, 1);
            if (responseEntity == null) {
                throw new IllegalArgumentException();
            } else if (responseEntity.getStatusCode().is4xxClientError()) {
                operationResult.setResultOK(false);
                operationResult.setErrorMessage(responseEntity.getBody());
                return new ResponseEntity<>(operationResult, responseEntity.getStatusCode());
            } else if (responseEntity.getStatusCode().is5xxServerError()) {
                operationResult.setResultOK(false);
                operationResult.setErrorMessage(responseEntity.getBody());
                return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
            }
            operationResult.setResultOK(true);
            operationResult.setErrorMessage("Se han almacenado correctamente los datos biometricos");
            return new ResponseEntity<>(operationResult, HttpStatus.OK);
        } catch (FeignException fe) {
            ResponseEntity<OperationResult> resultResponseEntity = parseFeign(fe);
            if (resultResponseEntity.getBody().getErrorMessage().equals("40003")) {
                ResponseEntity<String> responseEntity = null;
                try {
                    responseEntity = biometricClient.searchByFinger(operationRequestJSON.toString());
                    String bodyString = responseEntity.getBody();
                    JSONObject jsonObject = new JSONObject(bodyString);
                    String id = jsonObject.getString("id");
                    OperationResult operationResult1 = new OperationResult();
                    operationResult1.setResultOK(false);
                    JSONObject operationJsonResult = new JSONObject();
                    operationJsonResult.put("id", Long.valueOf(id));
                    operationJsonResult.put("status", "40003");
                    operationResult1.setErrorMessage(operationJsonResult.toString());
                    return new ResponseEntity<>(operationResult1, HttpStatus.UNPROCESSABLE_ENTITY);
                } catch (Exception e) {
                    log.error("Duplicated record, unable to find related one with message: {}", e.getMessage());
                    return resultResponseEntity;
                }
            } else if (resultResponseEntity.getBody().getErrorMessage().equals("40006")) {
                return new ResponseEntity<>(resultResponseEntity.getBody(), HttpStatus.PRECONDITION_FAILED);
            }
            {
                return resultResponseEntity;
            }
        } catch (Exception e) {
            log.error("Error in addMinucias for: {} - {} with message: {}", operationId, type, e.getMessage());
            operationResult.setResultOK(false);
            operationResult.setErrorMessage("40002");
            return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
        }

    }


    @Deprecated
    @ApiOperation(value = "Adds the binary information from the capture of the fingers. Expects a JSON like {'operationId' : 1, 'scanId' : 'currentScan' , 'documentId' : 'currentDocumentManagerId' , 'll' : 'x' , 'lr' : 'x', 'lm' : 'x' , 'li': 'x', 'lt' : 'x', 'rl' : 'x', 'rr' : 'x', 'rm' : 'x', 'ri' : 'x'} as first 'l' stands for left and  'r' for right and second letter stands for little, ring, middle, index and thumb")
    @RequestMapping(value = "/minucias", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public ResponseEntity<OperationResult> addMinucias(
            @RequestPart(value = "json") String jsonRequest, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{addMinucias() }");
        OperationResult operationResult = new OperationResult();
        JSONObject operationRequestJSON = null;
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        if (!validateJson(jsonRequest)) {
            operationResult.setErrorMessage("Bad Request");
            return new ResponseEntity<>(operationResult, HttpStatus.BAD_REQUEST);
        }
        operationRequestJSON = new JSONObject(jsonRequest);
        String type = operationRequestJSON.optString("type", "FINGERS");
        Long operationId = operationRequestJSON.getLong("operationId");
        try {
            String jsonToSend = updateRequest(jsonRequest);
            JSONObject jsonObject = new JSONObject(jsonToSend);
            jsonObject.put("username", username);
            ResponseEntity<String> responseEntity = biometricClient.addMinucias(jsonObject.toString(), operationId, 1);
            if (responseEntity == null) {
                throw new IllegalArgumentException();
            } else if (responseEntity.getStatusCode().is4xxClientError()) {
                operationResult.setResultOK(false);
                operationResult.setErrorMessage(responseEntity.getBody());
                return new ResponseEntity<>(operationResult, responseEntity.getStatusCode());
            } else if (responseEntity.getStatusCode().is5xxServerError()) {
                operationResult.setResultOK(false);
                operationResult.setErrorMessage(responseEntity.getBody());
                return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
            }
            operationResult.setResultOK(true);
            operationResult.setErrorMessage("Se han almacenado correctamente los datos biometricos");
            return new ResponseEntity<>(operationResult, HttpStatus.OK);
        } catch (FeignException fe) {
            log.debug("Error feign detected: {}", fe.getMessage());
            ResponseEntity<OperationResult> resultResponseEntity = parseFeign(fe);
            log.debug("Parsed feign: {}", resultResponseEntity);
            if (resultResponseEntity.getBody().getErrorMessage().equals("40003")) {
                ResponseEntity<String> responseEntity = null;
                try {
                    responseEntity = biometricClient.searchByFinger(jsonRequest);
                    String bodyString = responseEntity.getBody();
                    JSONObject jsonObject = new JSONObject(bodyString);
                    String id = jsonObject.getString("id");
                    OperationResult operationResult1 = new OperationResult();
                    operationResult1.setResultOK(false);
                    JSONObject operationJsonResult = new JSONObject();
                    operationJsonResult.put("id", Long.valueOf(id));
                    operationJsonResult.put("status", "40003");
                    operationResult1.setErrorMessage(operationJsonResult.toString());
                    return new ResponseEntity<>(operationResult1, HttpStatus.UNPROCESSABLE_ENTITY);
                } catch (Exception e) {
                    log.error("Duplicated record, unable to find related one with message: {}", e.getMessage());
                    return resultResponseEntity;
                }
            } else if (resultResponseEntity.getBody().getErrorMessage().equals("40006")) {
                OperationResult operationResult1 = new OperationResult();
                operationResult1.setResultOK(false);
                operationResult1.setErrorMessage("40006");
                return new ResponseEntity<>(operationResult1, HttpStatus.PRECONDITION_FAILED);
            } else {
                return resultResponseEntity;
            }
        } catch (Exception e) {
            log.error("Error in addMinucias for: {} - {} with message: {}", operationId, type, e.getMessage());
            operationResult.setResultOK(false);
            operationResult.setErrorMessage("40002");
            return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    private ResponseEntity<OperationResult> parseFeign(FeignException fe) {
    	//log.info("lblancas: "+this.getClass().getName()+".{parseFeign() }");
        OperationResult operationResult = new OperationResult();
        String errorTrace = fe.getMessage();
        String[] messageTrace = errorTrace.split("content:");
        if (messageTrace.length > 1) {
            String errorCode = messageTrace[messageTrace.length - 1];
            errorCode = errorCode.trim();
            try {
                Integer.parseInt(errorCode);
                operationResult.setResultOK(false);
                operationResult.setErrorMessage(errorCode);
                return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
            } catch (NumberFormatException ne) {
                log.error("No valid return error code from remote service for: {}", errorCode);
            }
        }
        operationResult.setResultOK(false);
        operationResult.setErrorMessage("40002");
        return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    private String updateRequest(String jsonRequest) {
    	//log.info("lblancas: "+this.getClass().getName()+".{updateRequest() }");
        try {
            JSONObject jsonToSend = new JSONObject(jsonRequest);
            Long operationId = jsonToSend.getLong("operationId");
            ResponseEntity<IneDetailDTO> responseIneDetail = identificationClient.findDetail(operationId);
            IneDetailDTO ineDetailDTO = responseIneDetail.getBody();
            DocumentPictureRequestDTO pictureRequestDTO = new DocumentPictureRequestDTO();
            pictureRequestDTO.setId(String.valueOf(operationId));
            pictureRequestDTO.setIsAnverso(true);
            pictureRequestDTO.setPersonalIdentificationNumber(ineDetailDTO.getClavElec());
            ResponseEntity<byte[]> responseEntityByte = facialClient.getImageFromReference(pictureRequestDTO);
            byte[] picture = responseEntityByte.getBody();
            jsonToSend.put("facial", Base64Utils.encodeToString(picture));
            return jsonToSend.toString();
        } catch (Exception e) {
            log.error("No face retrieved with error: {}", e.getMessage());
            return jsonRequest;
        }
    }


    @ApiOperation(value = "Finds the customer related to the given cyphered biometric data", notes = "The request must be a valid json like the following: {ll,li,lm,lr,lt,rl,ri,rm,rr,rt,contentType}", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The customer is found"),
            @ApiResponse(code = 404, message = "The customer is not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/search/customerCyphered", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<String> searchByFingerCyphered(@RequestBody String jsonStringRequest) {
    	//log.info("lblancas: "+this.getClass().getName()+".{searchByFingerCyphered() }");
        try {
            JSONObject source = new JSONObject(jsonStringRequest);
            source = updateJsonRequest(source, FIELDS_ALL_FINGERS);
            return biometricClient.searchByFinger(source.toString());
        } catch (FeignException fe) {
            return parseFeignErrorForSearch(fe);
        } catch (Exception e) {
            log.error("Error in searchByFinger with message: {}", e.getMessage());
            return new ResponseEntity<>(buildJsonErrorSearch(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @Deprecated
    @ApiOperation(value = "Finds the customer related to the given biometric data", notes = "The request must be a valid json like the following: {ll,li,lm,lr,lt,rl,ri,rm,rr,rt,contentType}", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The customer is found"),
            @ApiResponse(code = 404, message = "The customer is not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/search/customer", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<String> searchByFinger(@RequestBody String jsonStringRequest) {
    	//log.info("lblancas: "+this.getClass().getName()+".{searchByFinger() }");
        try {
            return biometricClient.searchByFinger(jsonStringRequest);
        } catch (FeignException fe) {
            return parseFeignErrorForSearch(fe);
        } catch (Exception e) {
            log.error("Error in searchByFinger with message: {}", e.getMessage());
            return new ResponseEntity<>(buildJsonErrorSearch(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    } 
    @ApiOperation(value = "Finds the customer related to the given cyphered biometric data", notes = "The request must be a valid json like the following: {ll,li,lm,lr,lt,rl,ri,rm,rr,rt,contentType,id}", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The customer is found"),
            @ApiResponse(code = 404, message = "The customer is not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/search/customerIdCyphered", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<String> searchByFingerAndIdCyphered(@RequestBody String jsonStringRequest) {
    	//log.info("lblancas: "+this.getClass().getName()+".{searchByFingerAndIdCyphered() } ::::"+jsonStringRequest);
        try {
            JSONObject source = new JSONObject(jsonStringRequest);
            source = updateJsonRequest(source, FIELDS_ALL_FINGERS);
            return biometricClient.searchByFingerAndId(source.toString());
        } catch (FeignException fe) {
            return parseFeignErrorForSearch(fe);
        } catch (Exception e) {
            log.error("Error in searchByFingerAndId with message: {}", e.getMessage());
            return new ResponseEntity<>("Error", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }


    @Deprecated
    @ApiOperation(value = "Finds the customer related to the given biometric data", notes = "The request must be a valid json like the following: {ll,li,lm,lr,lt,rl,ri,rm,rr,rt,contentType,id}", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The customer is found"),
            @ApiResponse(code = 404, message = "The customer is not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/search/customerId", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<String> searchByFingerAndId(@RequestBody String jsonStringRequest) {
    	//log.info("lblancas: "+this.getClass().getName()+".{searchByFingerAndId() }");
        try {
            return biometricClient.searchByFingerAndId(jsonStringRequest);
        } catch (FeignException fe) {
            return parseFeignErrorForSearch(fe);
        } catch (Exception e) {
            log.error("Error in searchByFingerAndId with message: {}", e.getMessage());
            return new ResponseEntity<>("Error", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }


    @ApiOperation(value = "Finds the customer related to the given cyphered biometric data", notes = "The request must be a valid json like the following: {id,sl,sr,st}", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The customer is found"),
            @ApiResponse(code = 404, message = "The customer is not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/search/customerSlapsCyphered", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<String> searchBySlapsCyphered(@RequestBody String jsonStringRequest) {
    	//log.info("lblancas: "+this.getClass().getName()+".{searchBySlapsCyphered() }");
        try {
            JSONObject source = new JSONObject(jsonStringRequest);
            source = updateJsonRequest(source, FIELDS_SLAPS);
            return biometricClient.searchBySlaps(source.toString());
        } catch (FeignException fe) {
            return parseFeignErrorForSearch(fe);
        } catch (Exception e) {
            log.error("Error in searchBySlaps with message: {}", e.getMessage());
            return new ResponseEntity<>("Error", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @Deprecated
    @ApiOperation(value = "Finds the customer related to the given biometric data", notes = "The request must be a valid json like the following: {id,sl,sr,st}", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The customer is found"),
            @ApiResponse(code = 404, message = "The customer is not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/search/customerSlaps", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<String> searchBySlaps(@RequestBody String jsonStringRequest) {
    	//log.info("lblancas: "+this.getClass().getName()+".{searchBySlaps() }");
        try {
            return biometricClient.searchBySlaps(jsonStringRequest);
        } catch (FeignException fe) {
            return parseFeignErrorForSearch(fe);
        } catch (Exception e) {
            log.error("Error in searchBySlaps with message: {}", e.getMessage());
            return new ResponseEntity<>("Error", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Finds the customer related to the given cyphered biometric data", notes = "The request must be a valid json like the following: {id,sl,sr,st,id}", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The customer is found"),
            @ApiResponse(code = 404, message = "The customer is not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/search/customerSlapsIdCyphered", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<String> searchBySlapsIdCyphered(@RequestBody String jsonStringRequest) {
    	//log.info("lblancas: "+this.getClass().getName()+".{searchBySlapsIdCyphered() }");
        try {
            JSONObject source = new JSONObject(jsonStringRequest);
            source = updateJsonRequest(source, FIELDS_SLAPS);
            return biometricClient.searchBySlapsId(source.toString());
        } catch (FeignException fe) {
            return parseFeignErrorForSearch(fe);
        } catch (Exception e) {
            log.error("Error in searchBySlapsId with message: {}", e.getMessage());
            return new ResponseEntity<>("Error", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @Deprecated
    @ApiOperation(value = "Finds the customer related to the given biometric data", notes = "The request must be a valid json like the following: {id,sl,sr,st,id}", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The customer is found"),
            @ApiResponse(code = 404, message = "The customer is not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/search/customerSlapsId", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<String> searchBySlapsId(@RequestBody String jsonStringRequest) {
    	//log.info("lblancas: "+this.getClass().getName()+".{searchBySlapsId() }");
        try {
            return biometricClient.searchBySlapsId(jsonStringRequest);
        } catch (FeignException fe) {
            return parseFeignErrorForSearch(fe);
        } catch (Exception e) {
            log.error("Error in searchBySlapsId with message: {}", e.getMessage());
            return new ResponseEntity<>("Error", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }


    @ApiOperation(value = "Finds the customer related to the given biometric data", notes = "The request must be a valid json like the following: {id,facial}", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The customer is found"),
            @ApiResponse(code = 404, message = "The customer is not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/search/customerFace", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<String> searchByFace(@RequestBody String jsonStringRequest) {
    	//log.info("lblancas: "+this.getClass().getName()+".{searchByFace() }");
        try {
            return biometricClient.searchByFace(jsonStringRequest);
        } catch (FeignException fe) {
            return parseFeignErrorForSearch(fe);
        } catch (Exception e) {
            log.error("Error in searchBySlapsId with message: {}", e.getMessage());
            return new ResponseEntity<>("Error", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Finds the customer related to the given biometric data", notes = "The request must be a valid json like the following: {id,facial}", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The customer is found"),
            @ApiResponse(code = 404, message = "The customer is not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/search/customerFaceCyphered", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<String> searchByFaceCyphered(@RequestBody String jsonStringRequest) {
    	//log.info("lblancas: "+this.getClass().getName()+".{searchByFaceCyphered() }");
        try {
            JSONObject source = new JSONObject(jsonStringRequest);
            source = updateJsonRequest(source, FIELDS_FACE);
            return biometricClient.searchByFace(source.toString());
        } catch (FeignException fe) {
            return parseFeignErrorForSearch(fe);
        } catch (Exception e) {
            log.error("Error in searchBySlapsId with message: {}", e.getMessage());
            return new ResponseEntity<>("Error", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Finds the customer related to the given biometric data", notes = "The request must be a valid json like the following: {id,facial}", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The customer is found"),
            @ApiResponse(code = 404, message = "The customer is not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/search/customerFaceId", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<String> searchByFaceId(@RequestBody String jsonStringRequest) {
    	//log.info("lblancas: "+this.getClass().getName()+".{searchByFaceId() }");
        try {
            return biometricClient.searchByFaceId(jsonStringRequest);
        } catch (FeignException fe) {
            return parseFeignErrorForSearch(fe);
        } catch (Exception e) {
            log.error("Error in searchBySlapsId with message: {}", e.getMessage());
            return new ResponseEntity<>("Error", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Finds the customer related to the given biometric data", notes = "The request must be a valid json like the following: {id,facial}", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The customer is found"),
            @ApiResponse(code = 404, message = "The customer is not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/search/customerFaceIdCyphered", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<String> searchByFaceIdCyphered(@RequestBody String jsonStringRequest) {
    	//log.info("lblancas: "+this.getClass().getName()+".{searchByFaceIdCyphered() }");
        try {
            JSONObject source = new JSONObject(jsonStringRequest);
            source = updateJsonRequest(source, FIELDS_FACE);
            return biometricClient.searchByFaceId(source.toString());
        } catch (FeignException fe) {
            return parseFeignErrorForSearch(fe);
        } catch (Exception e) {
            log.error("Error in searchBySlapsId with message: {}", e.getMessage());
            return new ResponseEntity<>("Error", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }


    @ApiOperation(value = "Starts process on client for verification on biometric system", response = String.class, notes = "Response 1 is error, response 0 is OK")
    @RequestMapping(value = "/startAuth", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public String initCaptureForIdentification(@RequestBody BiometricCaptureRequestIdentDTO requestIdentDTO) {
    	//log.info("lblancas: "+this.getClass().getName()+".{initCaptureForIdentification() }");
        try {
            biometricClient.initCaptureForIdentification(requestIdentDTO);
            return "0";
        } catch (Exception e) {
            log.error("Error for initAuthCapture for: {} with message: {}", requestIdentDTO, e.getMessage());
        }
        return "1";
    }

    @ApiOperation(value = "Starts process on client for sign contract on biometric system", response = String.class, notes = "Response 1 is error, response 0 is OK")
    @RequestMapping(value = "/startSign", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public String initCaptureSignForIdentification(@RequestBody BiometricCaptureRequestIdentToReceiveDTO requestIdentDTO) {
    	//log.info("lblancas: "+this.getClass().getName()+".{initCaptureSignForIdentification() }");
        try {
            BiometricCaptureRequestIdentDTO dto = new BiometricCaptureRequestIdentDTO(requestIdentDTO.getId(), requestIdentDTO.getSerial(), requestIdentDTO.getUsername(), "1");
            biometricClient.initCaptureSignForIdentification(dto);
            return "0";
        } catch (Exception e) {
            log.error("Error for initAuthCapture for: {} with message: {}", requestIdentDTO, e.getMessage());
        }
        return "1";
    }

    @ApiOperation(value = "Finds the status of the process for given serial number", response = BiometricCaptureRequestIdentDTO.class)
    @RequestMapping(value = "/statusAuth/{serial}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BiometricCaptureRequestIdentDTO> getStatusForAuth(@PathVariable String serial) {
    	//log.info("lblancas: "+this.getClass().getName()+".{getStatusForAuth() }");
        try {
            BiometricCaptureRequestIdentDTO dataFound = biometricClient.getStatusForAuth(serial);
            if (dataFound == null) {
                return new ResponseEntity<>((BiometricCaptureRequestIdentDTO) null, HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(dataFound, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error for getStatusForAuth for : {} with message: {}", serial, e.getMessage());
            return new ResponseEntity<>((BiometricCaptureRequestIdentDTO) null, HttpStatus.NOT_FOUND);
        }
    }

    @ApiOperation(value = "Finds the status of the process for contract sign based on given serial number", response = BiometricCaptureRequestIdentDTO.class)
    @RequestMapping(value = "/statusSign/{serial}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BiometricCaptureRequestIdentDTO> getStatusForAuthSign(@PathVariable String serial) {
    	//log.info("lblancas: "+this.getClass().getName()+".{getStatusForAuthSign() }");
        try {
            BiometricCaptureRequestIdentDTO dataFound = biometricClient.getStatusForAuthSign(serial);
            if (dataFound == null) {
                return new ResponseEntity<>((BiometricCaptureRequestIdentDTO) null, HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(dataFound, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error for getStatusForAuth for : {} with message: {}", serial, e.getMessage());
            return new ResponseEntity<>((BiometricCaptureRequestIdentDTO) null, HttpStatus.NOT_FOUND);
        }
    }

    @ApiOperation(value = "Confirms the current capture result for the associated serial number and customer id")
    @RequestMapping(value = "/confirmAuth/{status}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public String confirmCaptureAuth(@RequestBody BiometricCaptureRequestIdentToReceiveDTO requestIdentDTO, @PathVariable Integer status) {
    	//log.info("lblancas: "+this.getClass().getName()+".{confirmCaptureAuth() }");
        try {
            BiometricCaptureRequestIdentDTO dto = new BiometricCaptureRequestIdentDTO(requestIdentDTO.getId(), requestIdentDTO.getSerial(), requestIdentDTO.getUsername(), "1");
            return biometricClient.confirmCaptureAuth(dto, status);
        } catch (Exception e) {
            log.error("Error on confirmCaptureAuth for: {},{} with message: {}", requestIdentDTO, status, e.getMessage());
            return "1";
        }
    }

    @ApiOperation(value = "Confirms the current capture result for the associated serial number and customer id for contract sign")
    @RequestMapping(value = "/confirmSign/{status}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public String confirmCaptureSign(@RequestBody BiometricCaptureRequestIdentToReceiveDTO requestIdentDTO, @PathVariable Integer status) {
    	//log.info("lblancas: "+this.getClass().getName()+".{confirmCaptureSign() }");
        try {
            BiometricCaptureRequestIdentDTO dto = new BiometricCaptureRequestIdentDTO(requestIdentDTO.getId(), requestIdentDTO.getSerial(), requestIdentDTO.getUsername(), "1");
            return biometricClient.confirmCaptureSign(dto, status);
        } catch (Exception e) {
            log.error("Error on confirmCaptureAuth for: {},{} with message: {}", requestIdentDTO, status, e.getMessage());
            return "1";
        }
    }

    @ApiOperation(value = "Queries the results for the given data")
    @RequestMapping(value = "/queryAuth", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> queryCaptureAuth(@RequestBody BiometricCaptureRequestIdentToReceiveDTO requestIdentDTO) {
    	//log.info("lblancas: "+this.getClass().getName()+".{queryCaptureAuth() }");
        try {
            BiometricCaptureRequestIdentDTO dto = new BiometricCaptureRequestIdentDTO(requestIdentDTO.getId(), requestIdentDTO.getSerial(), requestIdentDTO.getUsername(), "1");
            String queryResult = biometricClient.queryCaptureAuth(dto);
            if (queryResult != null && queryResult.trim().equals("0")) {
                return new ResponseEntity<>("0", HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(queryResult, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Erorr on queryCaptureAuth for: {} with message: {}", requestIdentDTO, e.getMessage());
            return new ResponseEntity<>("0", HttpStatus.NOT_FOUND);
        }
    }

    @ApiOperation(value = "Queries the results for the given data")
    @RequestMapping(value = "/querySign", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> queryCaptureSigm(@RequestBody BiometricCaptureRequestIdentToReceiveDTO requestIdentDTO) {
    	//log.info("lblancas: "+this.getClass().getName()+".{queryCaptureSigm() }");
        try {
            BiometricCaptureRequestIdentDTO dto = new BiometricCaptureRequestIdentDTO(requestIdentDTO.getId(), requestIdentDTO.getSerial(), requestIdentDTO.getUsername(), "1");
            String queryResult = biometricClient.queryCaptureSign(dto);
            if (queryResult != null && queryResult.trim().equals("0")) {
                return new ResponseEntity<>("0", HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(queryResult, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Erorr on queryCaptureAuth for: {} with message: {}", requestIdentDTO, e.getMessage());
            return new ResponseEntity<>("0", HttpStatus.NOT_FOUND);
        }
    }

    @ApiOperation(value = "Compares two faces according to biometric templates if it passes the requested umbral(score)")
    @RequestMapping(value = "/compareFacial", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> compareFacial(@RequestBody CompareFacialRequestDTO compareFacialRequestDTO, HttpServletRequest request){
    	//log.info("lblancas: "+this.getClass().getName()+".{compareFacial() }");
        try{
            JSONObject jsonObject = new JSONObject(compareFacialRequestDTO);
            log.debug("requesting: {}", jsonObject);
            ResponseEntity<String> responseEntity = biometricClient.compareFaces(jsonObject.toString());
            return responseEntity;
        }catch (Exception e){
            log.error("Error looking for facial match with message: {}", e.getMessage());
            log.error("Trace: ", e);
            return new ResponseEntity<>((String) null, HttpStatus.NOT_FOUND);
        }
    }

    @ApiOperation(value = "Compares two faces according to biometric templates if it passes the requested umbral(score)")
    @RequestMapping(value = "/compareFacialCyphered", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> compareFacialCyphered(@RequestBody CompareFacialRequestDTO compareFacialRequestDTO, HttpServletRequest request){
    	//log.info("lblancas: "+this.getClass().getName()+".{ResponseEntity() }");
        try{
            String face1 = compareFacialRequestDTO.getB64Face1();
            String face2 = compareFacialRequestDTO.getB64Face2();
            face1 = decrypt.decrypt(face1);
            face2 = decrypt.decrypt(face2);
            compareFacialRequestDTO.setB64Face1(face1);
            compareFacialRequestDTO.setB64Face2(face2);
            JSONObject jsonObject = new JSONObject(compareFacialRequestDTO);
            log.debug("requesting: {}", jsonObject);
            ResponseEntity<String> responseEntity = biometricClient.compareFaces(jsonObject.toString());
            return responseEntity;
        }catch (Exception e){
            log.error("Error looking for facial match with message: {}", e.getMessage());
            log.error("Trace: ", e);
            return new ResponseEntity<>((String) null, HttpStatus.NOT_FOUND);
        }
    }


    private ResponseEntity<String> parseFeignErrorForSearch(FeignException fe) {
    	//log.info("lblancas: "+this.getClass().getName()+".{ResponseEntity() }");
        ResponseEntity<String> responseEntity;
        switch (fe.status()) {
            case 404:
                responseEntity = new ResponseEntity<>(buildJsonErrorSearch(), HttpStatus.NOT_FOUND);
                break;
            case 400:
                responseEntity = new ResponseEntity<>(buildJsonErrorSearch(), HttpStatus.BAD_REQUEST);
                break;
            case 500:
                responseEntity = new ResponseEntity<>(buildJsonErrorSearch(), HttpStatus.UNPROCESSABLE_ENTITY);
                break;
            default:
                responseEntity = new ResponseEntity<>(buildJsonErrorSearch(), HttpStatus.UNPROCESSABLE_ENTITY);
                break;
        }
        return responseEntity;
    }

    private String buildJsonErrorSearch() {
    	//log.info("lblancas: "+this.getClass().getName()+".{buildJsonErrorSearch() }");
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", "0");
        jsonObject.put("hasFingers", false);
        jsonObject.put("hasFacial", false);
        jsonObject.put("time", String.valueOf(System.currentTimeMillis()));
        return jsonObject.toString();
    }

    private JSONObject updateJsonRequest(JSONObject source, String[] fields) {
    	//log.info("lblancas: "+this.getClass().getName()+".{updateJsonRequest() }");
        for (String s : fields) {
            String cyphered = source.optString(s, null);
            if (cyphered != null) {
                String target = decrypt.decrypt(cyphered);
                source.put(s, new String(target));
            }
        }
        return source;
    }

}

package com.teknei.bid.controller.rest.unauth;

import com.teknei.bid.controller.rest.EnrollmentContractCertController;
import com.teknei.bid.controller.rest.EnrollmentContractController;
import com.teknei.bid.dto.ContractDemoDTO;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Base64;
import java.util.Base64.Decoder;

import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@RestController
@RequestMapping(value = "/rest/unauth/enrollment/contractCert")
@CrossOrigin
public class EnrollmentContractCertControllerUnauth {
	private static final Logger log = LoggerFactory.getLogger(EnrollmentContractCertControllerUnauth.class);
    @Autowired
    private EnrollmentContractCertController controller;
    @Autowired
    private EnrollmentContractController contractController;
    
    @ApiResponses({
            @ApiResponse(code = 403, message = "The certificate is not present for the current user"),
            @ApiResponse(code = 200, message = "The certificate is present and is valid")
    })
    @ApiOperation(value = "Generates the contract depending on whether the user cert is present or not")
    @RequestMapping(value = "/certContract/{idCustomer}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getContractWithCert(@PathVariable Long idCustomer, HttpServletRequest request){
    	//log.info("lblancas: "+this.getClass().getName()+".{getContractWithCert(GET) }");
        return controller.getContractWithCert(idCustomer, request);
    }
    @ApiOperation(value = "Generates the contract depending on whether the user cert is present or not")
    @RequestMapping(value = "/certContractDemo/{nombre}/{curp}/{firma}/{domicilio}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getContrac(@PathVariable String nombre,
    		@PathVariable String curp, @PathVariable boolean firma,
    		@PathVariable String domicilio, HttpServletRequest request)
    {
    	 //log.info("lblancas: "+this.getClass().getName()+".{getContrac(GET) }");
    	 ContractDemoDTO dto =new  ContractDemoDTO ();
    	 dto.setCurp(curp);
    	 dto.setNombre(nombre);
    	 dto.setDomicilio(domicilio);
    	 dto.setHuella("x");
    	 dto.setConhuella(firma);
    	 ResponseEntity<String> entity = (contractController.getContractDemo(dto,request)); 
    	 String resultado = entity.getBody(); 
    	 Decoder b64=Base64.getDecoder();
    	 byte[] res=b64.decode(resultado); 
    	 ResponseEntity<byte[] > entityB= new ResponseEntity<>(res, HttpStatus.OK); 
    	 return entityB;

    }
}

package com.teknei.bid.controller.rest.unauth;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.teknei.bid.controller.rest.EnrollmentContractController;
import com.teknei.bid.dto.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
@RestController
@RequestMapping(value = "/rest/unauth/enrollment/contract")
@CrossOrigin
public class EnrollmentContractControllerUnauth {
	private static final Logger log = LoggerFactory.getLogger(EnrollmentContractControllerUnauth.class);
    @Autowired
    private EnrollmentContractController contractController;


    @ApiOperation(value = "Stores the customer credentials related to the account and the contract generation", response = Byte.class)
    @RequestMapping(value = "/credentials", method = RequestMethod.POST)
    public ResponseEntity<Boolean> addCustomerCredentials(@RequestBody CustomerCredentials credentials){
    	//log.info("lblancas: "+this.getClass().getName()+".{addCustomerCredentials(POST) }");
        return contractController.addCustomerCredentials(credentials);
    }

    @ApiOperation(value = "Generates and gets the contract for the current operation", response = byte[].class)
    @RequestMapping(value = "/contrato/{id}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getUnsignedContract(@PathVariable("id") Long id, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{getUnsignedContract(GET) }");
        return contractController.getUnsignedContract(id, request);
    }

    @ApiOperation(value = "Sign the contract with the cyphered fingerprint. Verifies the identify of the customer", response = String.class)
    @RequestMapping(value = "/contrato/signCypheredJpg", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> signContractCypheredJpg(@RequestBody SignContractRequestDTO requestDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{signContractCyphered(POST) }");
        return contractController.signContractCypheredjpg(requestDTO, request);
    }

    @ApiOperation(value = "Sign the contract with the cyphered fingerprint. Verifies the identify of the customer", response = String.class)
    @RequestMapping(value = "/contrato/signCyphered", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> signContractCyphered(@RequestBody SignContractRequestDTO requestDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{signContractCyphered(POST) }");
        return contractController.signContractCyphered(requestDTO, request);
    }

    @Deprecated
    @ApiOperation(value = "Sign the contract with the fingerprint. Verifies the identify of the customer", response = String.class)
    @RequestMapping(value = "/contrato/sign", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> signContract(@RequestBody SignContractRequestDTO requestDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{signContract(POST) }");
        return contractController.signContract(requestDTO, request);
    }

    @ApiOperation(value = "Generates and gets the contract for the current operation", response = String.class)
    @RequestMapping(value = "/contratoB64/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getUnsignedContractB64(@PathVariable("id") Long id, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{getUnsignedContractB64(GET) }");
        return contractController.getUnsignedContractB64(id, request);
    }
    

    
    @ApiOperation(value = "Verifies if the customer has any previous record associated", 
			notes = "The response should contain only one record in the array", 
		response = ContractDemoDTO.class)
	@RequestMapping(value = "/contrato/contratoDemo", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, 
		produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public  ResponseEntity<String>  getContractDemo(@RequestBody ContractDemoDTO dto,HttpServletRequest request) 
	{
		//log.info("lblancas:[1] "+this.getClass().getName()+".{getContractDemo() }");
		return contractController.getContractDemo(dto,request);
	}
    
    
    
    @ApiOperation(value = "Adds the signed contract to the case file. It expects the file as attachment and the operationId in the URL", response = OperationResult.class)
    @RequestMapping(value = "/contrato/add/{id}", method = RequestMethod.POST)
    public ResponseEntity<OperationResult> addSignedContract(@RequestPart(value = "file") MultipartFile file, @PathVariable("id") Long id) {
    	//log.info("lblancas: "+this.getClass().getName()+".{addSignedContract(POST) }");
        return contractController.addSignedContract(file, id);
    }

    @ApiOperation(value = "Uploads contract signed and stores in the document manager", notes = "The content should be ciphered", response = OperationResult.class)
    @RequestMapping(value = "/uploadPlain", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> addPlainSignedContract(@RequestBody ContractSignedDTO contractSignedDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{addPlainSignedContract(POST) }");
        return contractController.addPlainSignedContract(contractSignedDTO, request);
    }

    @ApiOperation(value = "Finds the values that must be accepted by the customer", response = AcceptancePerCustomerDTO.class)
    @RequestMapping(value = "/acceptancePerCustomer", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<AcceptancePerCustomerDTO>> getAcceptancesPerCustomer(@RequestBody OperationIdDTO operationIdDTO, HttpServletRequest request){
    	//log.info("lblancas: "+this.getClass().getName()+".{getAcceptancesPerCustomer(POST) }");
        return contractController.getAcceptancesPerCustomer(operationIdDTO, request);
    }

    @RequestMapping(value = "/acceptance/acceptancePerCustomer", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> updateAcceptancePerCustomer(@RequestBody List<AcceptancePerCustomerDTO> dtoList, HttpServletRequest request){
    	//log.info("lblancas: "+this.getClass().getName()+".{updateAcceptancePerCustomer(PUT) }");
        return contractController.updateAcceptancePerCustomer(dtoList, request);
    }


    @ApiOperation(value = "Generates, pre fills and gets the contract for the current operation", response = byte[].class)
    @RequestMapping(value = "/contractPrefilled/{id}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getContractPrefilled(@PathVariable("id") Long id, HttpServletRequest request){
    	//log.info("lblancas: "+this.getClass().getName()+".{getContractPrefilled(GET) }");
        return contractController.getContractPrefilled(id, request);
    }
    
}

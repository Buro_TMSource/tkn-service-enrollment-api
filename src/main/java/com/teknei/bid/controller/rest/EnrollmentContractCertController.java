package com.teknei.bid.controller.rest;

import com.teknei.bid.dto.OperationIdDTO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/rest/v3/enrollment/contractCert")
@CrossOrigin
public class EnrollmentContractCertController {

    private static final Logger log = LoggerFactory.getLogger(EnrollmentContractCertController.class);

    @Value("${tkn.sbd.generateCert}")
    private Boolean generateCert;

    @Autowired
    private EnrollmentCertController certController;
    @Autowired
    private EnrollmentContractController contractController;
    @Autowired
    private TaskExecutor taskExecutor;

    @ApiResponses({
            @ApiResponse(code = 403, message = "The certificate is not present for the current user"),
            @ApiResponse(code = 200, message = "The certificate is present and is valid")
    })
    @ApiOperation(value = "Generates the contract depending on whether the user cert is present or not")
    @RequestMapping(value = "/certContract/{idCustomer}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getContractWithCert(@PathVariable Long idCustomer, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{getContractWithCert() }");
        OperationIdDTO operationIdDTO = new OperationIdDTO();
        operationIdDTO.setOperationId(idCustomer);
        //TODO check if viable to send in another thread line below
        //startCertGeneration(idCustomer, request);
        //ResponseEntity<String> responseEntity = certController.validateCert//(idCustomer);
        //ResponseEntity<String> responseEntity = certController.generateCert(operationIdDTO, request);
        //if (responseEntity.getStatusCode().is2xxSuccessful() || responseEntity.getStatusCodeValue() == 409) {
            return contractController.getUnsignedContract(idCustomer, request);
        //}
        //return new ResponseEntity<>((byte[]) null, HttpStatus.FORBIDDEN);
        //return contractController.getUnsignedContract(idCustomer, request);
    }

    private void startCertGeneration(Long idCustomer, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{startCertGeneration() }");
        if (generateCert) {
            Runnable runnable = () -> {
                try {
                    OperationIdDTO operationIdDTO = new OperationIdDTO();
                    operationIdDTO.setOperationId(idCustomer);
                    ResponseEntity<String> responseEntity = certController.generateCert(operationIdDTO, request);
                    if (responseEntity == null || responseEntity.getStatusCode().is4xxClientError() || responseEntity.getStatusCode().is5xxServerError()) {
                        log.error("Error generating certificate for customer: {}", idCustomer);
                    } else {
                        log.debug("Serial generation success");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            };
            taskExecutor.execute(runnable);
        } else {
            log.info("No active configuration for generate certificate");
        }
    }

}

package com.teknei.bid.controller.rest;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.teknei.bid.dto.ClientDetailDTO;
import com.teknei.bid.dto.DocumentPictureRequestDTO;
import com.teknei.bid.service.remote.AddressClient;
import com.teknei.bid.service.remote.CustomerClient;
import com.teknei.bid.service.remote.FacialClient;
import com.teknei.bid.service.remote.IdentificationClient;
import com.teknei.bid.service.remote.VideocallClient;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/rest/v3/enrollment/pictures")
@CrossOrigin
public class EnrollmentPicturesController {

    @Autowired
    private AddressClient addressClient;
    @Autowired
    private IdentificationClient identificationClient;
    @Autowired
    private FacialClient facialClient;
    @Autowired
    private CustomerClient customerClient;
    @Autowired
    private VideocallClient videocallClient;
    private static final Logger log = LoggerFactory.getLogger(EnrollmentPicturesController.class);


    @ApiOperation(value = "Gets the image corresponding for the requestd params", notes = "The response will be a JSON with format: {'image' : 'content in b64'}. The options available are: {1=anverseId, 2=reverseId, 3=facial, 4=addressComprobant, 5=selfie}", response = String.class)
    @RequestMapping(value = "/search/customer/imageB64/{option}/{curp}/{id}", method = RequestMethod.GET, produces = MediaType.
            APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> findPictureFromReferenceB64(@PathVariable Integer option, @PathVariable String curp, @PathVariable String id) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findPictureFromReferenceB64() }");
        try {	
            ResponseEntity<ClientDetailDTO> responseEntity = customerClient.findDetailCurp(curp);
            String personalNumber = responseEntity.getBody().getPersonalNumber();
            DocumentPictureRequestDTO requestDTO = new DocumentPictureRequestDTO();
            requestDTO.setId(id);
            requestDTO.setIsAnverso(true);
            requestDTO.setPersonalIdentificationNumber(personalNumber);
            ResponseEntity<byte[]> responseEntityImage;
            switch (option) {
                case 1:
                    responseEntityImage = identificationClient.getImageFromReference(requestDTO);
                    break;
                case 2:
                    requestDTO.setIsAnverso(false);
                    responseEntityImage = identificationClient.getImageFromReference(requestDTO);
                    break;
                case 3:
                    responseEntityImage = facialClient.getImageFromReference(requestDTO);
                    break;
                case 4:
                    responseEntityImage = addressClient.getImageFromReference(requestDTO);
                    break;
                case 5:
                    responseEntityImage = videocallClient.getImageFromReference(requestDTO);
                    break;
                default:
                    responseEntityImage = identificationClient.getImageFromReference(requestDTO);
                    break;

            }
            if (responseEntityImage.getStatusCode().equals(HttpStatus.OK)) {
                byte[] contentByte = responseEntityImage.getBody();
                String b64Content = Base64Utils.encodeToString(contentByte);
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("image", b64Content);
                return new ResponseEntity<>(jsonObject.toString(), HttpStatus.OK);
            }
            return new ResponseEntity<>((String) null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            log.error("Error in findPictureFromReference for: {} , {} , {} with message: {}", option, curp, id, e.getMessage());
            return new ResponseEntity<>((String) null, HttpStatus.NOT_FOUND);
        }
    }

    @ApiOperation(value = "Gets the image corresponding for the requestd params")
    @RequestMapping(value = "/search/customer/image/{option}/{curp}/{id}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> findPictureFromReference(@PathVariable Integer option, @PathVariable String curp, @PathVariable String id) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findPictureFromReference() }");
        try {
            ResponseEntity<ClientDetailDTO> responseEntity = customerClient.findDetailCurp(curp);
            String personalNumber = responseEntity.getBody().getPersonalNumber();
            DocumentPictureRequestDTO requestDTO = new DocumentPictureRequestDTO();
            requestDTO.setId(id);
            requestDTO.setIsAnverso(true);
            requestDTO.setPersonalIdentificationNumber(personalNumber);
            ResponseEntity<byte[]> responseEntityImage;
            switch (option) {
                case 1:
                    responseEntityImage = identificationClient.getImageFromReference(requestDTO);
                    break;
                case 2:
                    requestDTO.setIsAnverso(false);
                    responseEntityImage = identificationClient.getImageFromReference(requestDTO);
                    break;
                case 3:
                    responseEntityImage = facialClient.getImageFromReference(requestDTO);
                    break;
                case 4:
                    responseEntityImage = addressClient.getImageFromReference(requestDTO);
                    break;
                default:
                    responseEntityImage = identificationClient.getImageFromReference(requestDTO);
                    break;

            }
            return responseEntityImage;
        } catch (Exception e) {
            log.error("Error in findPictureFromReference for: {} , {} , {} with message: {}", option, curp, id, e.getMessage());
            return new ResponseEntity<>((byte[]) null, HttpStatus.NOT_FOUND);
        }
    }

}

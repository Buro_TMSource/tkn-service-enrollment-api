package com.teknei.bid.controller.rest.unauth;

import com.teknei.bid.controller.rest.EnrollmentClientAccountController;
import com.teknei.bid.dto.BidClieCtaDest;
import com.teknei.bid.dto.BidClientAccountDestinyDTO;
import com.teknei.bid.dto.BidCreditInstitutionRequest;
import com.teknei.bid.dto.BidInstCred;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/unauth/enrollment/client/account")
@CrossOrigin
public class EnrollmentClientAccountControllerUnauth {
	private static final Logger log = LoggerFactory.getLogger(EnrollmentClientAccountControllerUnauth.class);
    @Autowired
    private EnrollmentClientAccountController controller; 

    @ApiOperation(value = "Finds the accounts related to the customer")
    @RequestMapping(value = "/accountDestiny/{idClient}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<BidClieCtaDest>> findRelatedAccounts(@PathVariable Long idClient) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findRelatedAccounts() }");
        return controller.findRelatedAccounts(idClient);
    }

    @ApiOperation(value = "Finds the accounts related to the customer, only inactive ones")
    @RequestMapping(value = "/accountDestinyInactive/{idClient}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<BidClieCtaDest>> findRelatedAccountsInactive(@PathVariable Long idClient) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findRelatedAccountsInactive() }");
        return controller.findRelatedAccountsInactive(idClient);
    }

    @ApiOperation(value = "Saves or updates account destiny information for the related customer. Identifiers are required, boolean fields are recommended for expected behavior", response = BidClieCtaDest.class)
    @RequestMapping(value = "/accountDestiny", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<BidClieCtaDest> saveCtaDest(@RequestBody BidClientAccountDestinyDTO request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{saveCtaDest() }");
        return controller.saveCtaDest(request);
    }

    @ApiOperation(value = "Gets all credit institution records", response = List.class)
    @RequestMapping(value = "/creditInstitution", method = RequestMethod.GET)
    public ResponseEntity<List<BidInstCred>> findInstCred() {
    	//log.info("lblancas: "+this.getClass().getName()+".{findInstCred() }");
        return controller.findInstCred();
    }

    @ApiOperation(value = "Saves or updates new credit institution based on request values. Send only name values when new record is needed, all fields are required when update is request", response = BidInstCred.class)
    @RequestMapping(value = "/creditInstitution", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<BidInstCred> saveInstCred(@RequestBody BidCreditInstitutionRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{saveInstCred() }");
        return controller.saveInstCred(request);
    }

}

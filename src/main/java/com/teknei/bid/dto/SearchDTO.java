package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class SearchDTO implements Serializable {

    private String curp;

}
package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class BidEmprDisp implements Serializable {
    private Long idEmpr;
    private Long idDisp;
}

package com.teknei.bid.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BidIfeValidationDTO implements Serializable {

    private Long id;
    private String nomb;
    private String apePat;
    private String apeMat;
    private String clavElec;
    private String ocr;
    private String mrz;
    private String curp;

}
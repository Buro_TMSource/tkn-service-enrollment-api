package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class BidDispDTO implements Serializable {

    private Long idDisp;
    private String descDisp;
    private String numSeri;
    private String username;
}
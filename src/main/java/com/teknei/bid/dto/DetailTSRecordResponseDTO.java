package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class DetailTSRecordResponseDTO implements Serializable {

    private Long id;
    private String curp;
    private Timestamp credentials;
    private String credentialsStr;
    private Timestamp facial;
    private String facialStr;
    private Timestamp address;
    private String addressStr;
    private Timestamp fingers;
    private String fingersStr;
    private Timestamp contract;
    private String contractStr;
}

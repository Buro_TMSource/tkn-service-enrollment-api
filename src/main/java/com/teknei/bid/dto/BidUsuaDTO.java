package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class BidUsuaDTO implements Serializable{

    private Long idUsua;
    private String usua;
    private String password;
    private String username;

}
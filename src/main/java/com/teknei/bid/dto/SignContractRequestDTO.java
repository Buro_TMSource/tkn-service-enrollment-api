package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class SignContractRequestDTO implements Serializable {

    private Long operationId;
    private String base64Finger;
    private String contentType;
    private String username;
    private String hash;
	@Override
	public String toString() {
		return "SignContractRequestDTO [operationId=" + operationId + ", base64Finger=" + base64Finger
				+ ", contentType=" + contentType + ", username=" + username + ", hash=" + hash + "]";
	}
    
}
package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
public class BidClieCtaDest implements Serializable {
    private Long idClie;
    private Long idInstCred;
    private String ctaClabDest;
    private String aliaCta;
    private BigDecimal montMax;
    private Integer idEsta;
    private Integer idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;
    private String holder;

}

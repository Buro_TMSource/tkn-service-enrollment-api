package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CipherRequestDTO implements Serializable {

    private String userRequest;
    private String passwordRequest;
    private String plainText;

}
package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class DetailTSRecordDTO implements Serializable {

    private Long id;
    private String curp;
    private Timestamp credentials;
    private Timestamp facial;
    private Timestamp address;
    private Timestamp fingers;
    private Timestamp contract;
}

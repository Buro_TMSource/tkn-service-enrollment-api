package com.teknei.bid.dto;
import lombok.AllArgsConstructor;
import lombok.Data;
@Data
@AllArgsConstructor
public class BidServiceConfiguration {
	private long   operationId;
	private int    settingsversion;   
	private String urlidscan;       
	private String licenseidscan;    
	private String urlteknei;      
	private String urlmobbsign;      
	private String licenseobbsign;   
	private String urlauthaccess;    
	private String fingerprintreader; 
	private String idcompany;  
	private String timeoutservices;   
	private String timelatency;       
	private String urlhostbid;        
	private String urlportbid;        
	private String status;  
}

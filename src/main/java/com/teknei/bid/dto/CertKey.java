package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CertKey implements Serializable {

    private String key;
    private String customerName;
    private String customerId;
    private String customerMail;
    private String usernameRequesting;

}
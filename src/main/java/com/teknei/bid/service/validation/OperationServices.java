package com.teknei.bid.service.validation;

import com.teknei.bid.dto.StartOperationResult;
import com.teknei.bid.util.EnrollmentVerificationSingleton;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Amaro on 07/08/2017.
 */
@Service
public class OperationServices {

    //TODO get required fields from configuration
	private static final Logger log = LoggerFactory.getLogger(OperationServices.class);
    public StartOperationResult validateStartOperation(JSONObject customerData) throws IllegalArgumentException {
    	//log.info("lblancas: "+this.getClass().getName()+".{validateStartOperation() }");
        StartOperationResult startOp = new StartOperationResult();
        List<String> requiredFields = Arrays.asList("deviceId", "employee", "curp", "nombre", "primerApellido", "segundoApellido", "email", "emprId", "customerType", "confType");
        requiredFields.forEach(f -> {
            if (!customerData.has(f)) {
                throw new IllegalArgumentException(String.format("Error al iniciar el proceso de un nuevo enrolamiento. No se ha enviado el parámetro '%s'.", f));
            }
        });
        if (!customerData.has("telefono") && !customerData.has("celular")) {
            throw new IllegalArgumentException(String.format("Error al iniciar el proceso de un nuevo enrolamiento. No se ha enviado el parámetro '%s'.", "telefono o celular"));
        }
        startOp.setErrorMessage(String.format("Se ha iniciado el proceso de un nuevo enrolamiento: '%s'", customerData.getString("curp")));
        startOp.setResultOK(true);
        return startOp;
    }

}

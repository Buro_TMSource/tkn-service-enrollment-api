package com.teknei.bid.service.remote.impl;

import com.teknei.bid.service.remote.Invoker;
import com.teknei.bid.util.EnrollmentVerificationSingleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Component
public class IdentificationAttachmentsClient {
	private static final Logger log = LoggerFactory.getLogger(IdentificationAttachmentsClient.class);
    //@Value(value = "${tkn.feign.identification-url}")
    //private String urlRemote;
    @Value(value = "${tkn.feign.identification-name}")
    private String serviceName;

    @Autowired
    private DiscoveryClient discoveryClient;

    public ResponseEntity<String> uploadIdentification(List<MultipartFile> files, Long id) {
    	//log.info("lblancas: "+this.getClass().getName()+".{uploadIdentification() }");
        return Invoker.execute(Invoker.findUrl(serviceName, "identification", discoveryClient), id, files);
    }

    public ResponseEntity<String> uploadAdditonalIdentification(List<MultipartFile> files, Long id) {
    	//log.info("lblancas: "+this.getClass().getName()+".{uploadAdditonalIdentification() }");
        return Invoker.execute(Invoker.findUrl(serviceName, "identification", discoveryClient), id, files, "uploadAdditional");
    }

    public ResponseEntity<String> uploadIdentificationValidation(List<MultipartFile> files, Long id) {
    	//log.info("lblancas: "+this.getClass().getName()+".{uploadIdentificationValidation() }");
        return Invoker.execute(Invoker.findUrl(serviceName, "identification", discoveryClient), id, files, "uploadCapture");
    }

}

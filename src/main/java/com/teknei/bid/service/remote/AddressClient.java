package com.teknei.bid.service.remote;

import com.teknei.bid.dto.AddressDetailDTO;
import com.teknei.bid.dto.DocumentPictureRequestDTO;
import com.teknei.bid.dto.RequestEncFilesDTO;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "${tkn.feign.address-name}")
public interface AddressClient {

    @RequestMapping(value = "/address/find/{id}", method = RequestMethod.GET)
    ResponseEntity<String> findById(@PathVariable("id") Long id);

    @RequestMapping(value = "/address/download", method = RequestMethod.POST)
    ResponseEntity<byte[]> getImageFromReference(@RequestBody DocumentPictureRequestDTO dto);

    @RequestMapping(value = "/address/updateManually/{id}/{type}", method = RequestMethod.POST)
    ResponseEntity<AddressDetailDTO> updateManually(@RequestBody AddressDetailDTO addressDetailDTO, @PathVariable("id") Long id, @PathVariable("type") Integer type);

    @RequestMapping(value = "/address/findDetail/{id}", method = RequestMethod.GET)
    ResponseEntity<AddressDetailDTO> findDetailById(@PathVariable("id") Long id);

    @RequestMapping(value = "/address/uploadPlain", method = RequestMethod.POST)
    ResponseEntity<String> uploadAddressDocument(@RequestBody RequestEncFilesDTO dto);

    @RequestMapping(value = "/address/uploadPlainParse", method = RequestMethod.POST)
    ResponseEntity<String> addAddressDocumentParsePlain(@RequestBody RequestEncFilesDTO dto);

}
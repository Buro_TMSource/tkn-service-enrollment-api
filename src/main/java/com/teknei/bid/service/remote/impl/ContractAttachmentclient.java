package com.teknei.bid.service.remote.impl;

import com.teknei.bid.service.remote.Invoker;
import com.teknei.bid.util.EnrollmentVerificationSingleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;

@Component
public class ContractAttachmentclient {
	private static final Logger log = LoggerFactory.getLogger(ContractAttachmentclient.class);
    //@Value(value = "${tkn.feign.contract-url}")
    //private String remoteUrl;
    @Value(value = "${tkn.feign.contract-name}")
    private String serviceName;

    @Autowired
    private DiscoveryClient discoveryClient;

    public ResponseEntity<String> addSignedContract(MultipartFile file, Long id) 
    {
    	//log.info("lblancas: "+this.getClass().getName()+".{addSignedContract() }");
        return Invoker.execute(Invoker.findUrl(serviceName, "contract", discoveryClient), id, Arrays.asList(file));
        //return Invoker.execute(remoteUrl, id, Arrays.asList(file));
    }

}
